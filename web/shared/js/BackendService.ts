class BackendService {
    API_URL: string;

    constructor(API_URL: string) {
        this.API_URL = API_URL;
    }

    private request(url: string, type: ReqTypes, data?: Object) {
        let req = new XMLHttpRequest();
        let fullURL = this.API_URL + url;
        data = JSON.stringify(data || {});
        return new Promise((resolve, reject) => {
            req.open(ReqTypes[type], fullURL, true);
            if (type == ReqTypes.POST) req.setRequestHeader("Content-Type", "application/json");
            req.onload = function () {
                // This is called even on 404 etc
                // so check the status
                if (req.status == 200) {
                    try {
                        let response = JSON.parse(req.response);
                        resolve(response);
                    } catch (e) {
                        resolve(e);
                    }
                } else if (req.status == 401) {
                    alert("You are not logged in!");
                    window.location="/";
                }
                else {
                    // Resolve the promise with the response text
                    try {
                        let response = JSON.parse(req.response);
                        resolve(response);
                    } catch (e) {
                        console.error(e);
                        console.log(req.response);
                        alert("Something went wrong! Please try again!");
                        reject(e);
                    }
                }
            };
            // Handle network errors
            req.onerror = function () {
                console.error(Error("Network Error"));
                alert("Please check your connection and try again!");
                reject(Error("Network Error"));
            };
            // Make the request
            req.send(data);
        });
    }

    //Log User out
    public logout() {
        return this.request("/logout", ReqTypes.POST);
    }

    public post(request: string, data: any) {
        return this.request(request, ReqTypes.POST, data);
    }

    public get(request: string) {
        return this.request(request, ReqTypes.GET);
    }
}

enum ReqTypes{
    POST,
    GET
}

class EMail {
    email: string;

    constructor(email: string) {
        this.email = email;
    }
}

class Cat_notes {
    id: string;
    name: string;
}

class Note {
    id: string;
    f_teacher: number;
    f_student: number;
    f_category: number;
    text: string;
    excused: boolean;


    constructor(id: string, f_teacher: number, f_student: number, f_category: number, text: string, excused: boolean) {
        this.id = id;
        this.f_teacher = f_teacher;
        this.f_student = f_student;
        this.f_category = f_category;
        this.text = text;
        this.excused = excused;
    }
}

class PasswordSet {
    password: string;
    newPassword: string;

    constructor(password: string, newPassword: string) {
        this.password = password;
        this.newPassword = newPassword;
    }
}

class Student {
    id: string;
    prename: string;
    surname: string;
    f_class: number;

    constructor(id: string, prename: string, surname: string, f_class: number) {
        this.id = id;
        this.prename = prename;
        this.surname = surname;
        this.f_class = f_class;
    }
}

class Teacher {
    id: string;
    prename: string;
    surname: string;
    username: string;
    email: string;
    admin: boolean;

    constructor(id: string, prename: string, surname: string, username: string, email: string, admin: boolean) {
        this.id = id;
        this.prename = prename;
        this.surname = surname;
        this.username = username;
        this.email = email;
        this.admin = admin;
    }
}

class Class {
    id: string;
    level: number;
    classNR: string;
    f_teacher: number;

    constructor(id: string, level: number, classNR: string, f_teacher: number) {
        this.id = id;
        this.level = level;
        this.classNR = classNR;
        this.f_teacher = f_teacher;
    }
}