class BackendService {
    constructor(API_URL) {
        this.API_URL = API_URL;
    }
    request(url, type, data) {
        let req = new XMLHttpRequest();
        let fullURL = this.API_URL + url;
        data = JSON.stringify(data || {});
        return new Promise((resolve, reject) => {
            req.open(ReqTypes[type], fullURL, true);
            if (type == ReqTypes.POST)
                req.setRequestHeader("Content-Type", "application/json");
            req.onload = function () {
                // This is called even on 404 etc
                // so check the status
                if (req.status == 200) {
                    try {
                        let response = JSON.parse(req.response);
                        resolve(response);
                    }
                    catch (e) {
                        resolve(e);
                    }
                }
                else if (req.status == 401) {
                    alert("You are not logged in!");
                    window.location = "/";
                }
                else {
                    // Resolve the promise with the response text
                    try {
                        let response = JSON.parse(req.response);
                        resolve(response);
                    }
                    catch (e) {
                        console.error(e);
                        console.log(req.response);
                        alert("Something went wrong! Please try again!");
                        reject(e);
                    }
                }
            };
            // Handle network errors
            req.onerror = function () {
                console.error(Error("Network Error"));
                alert("Please check your connection and try again!");
                reject(Error("Network Error"));
            };
            // Make the request
            req.send(data);
        });
    }
    //Log User out
    logout() {
        return this.request("/logout", ReqTypes.POST);
    }
    post(request, data) {
        return this.request(request, ReqTypes.POST, data);
    }
    get(request) {
        return this.request(request, ReqTypes.GET);
    }
}
var ReqTypes;
(function (ReqTypes) {
    ReqTypes[ReqTypes["POST"] = 0] = "POST";
    ReqTypes[ReqTypes["GET"] = 1] = "GET";
})(ReqTypes || (ReqTypes = {}));
class EMail {
    constructor(email) {
        this.email = email;
    }
}
class Cat_notes {
}
class Note {
    constructor(id, f_teacher, f_student, f_category, text, excused) {
        this.id = id;
        this.f_teacher = f_teacher;
        this.f_student = f_student;
        this.f_category = f_category;
        this.text = text;
        this.excused = excused;
    }
}
class PasswordSet {
    constructor(password, newPassword) {
        this.password = password;
        this.newPassword = newPassword;
    }
}
class Student {
    constructor(id, prename, surname, f_class) {
        this.id = id;
        this.prename = prename;
        this.surname = surname;
        this.f_class = f_class;
    }
}
class Teacher {
    constructor(id, prename, surname, username, email, admin) {
        this.id = id;
        this.prename = prename;
        this.surname = surname;
        this.username = username;
        this.email = email;
        this.admin = admin;
    }
}
class Class {
    constructor(id, level, classNR, f_teacher) {
        this.id = id;
        this.level = level;
        this.classNR = classNR;
        this.f_teacher = f_teacher;
    }
}
//# sourceMappingURL=BackendService.js.map