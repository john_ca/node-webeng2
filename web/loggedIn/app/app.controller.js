/**
 * Created by John on 12.07.2016.
 */
angular.module('myApp')
    .controller("appCtrl", function ($scope ,$rootScope) {
        $scope.logout = function () {
            $rootScope.bEmulator
                .logout()
                .then(function (res) {
                    if (res.error == false) window.location="/";
                });
        }
    });