function dashboardCtrl($rootScope, $scope) {
    var self = this;
    var bE;

    this.excuse = function (pNote) {
        bE.post("excuseNote", pNote)
            .then(function () {
                updateData();
                $scope.$digest();
            });
    };

    function updateData() {
        bE.get("getLastNotes").then(function (res) {
            self.lastNotes = res;
            $scope.$digest();
        });
        bE.get("getLastNotesOfClass").then(function (res) {
            self.lastNotesOfClass = res;
            $scope.$digest();
        });
    }

    function init() {
        bE = $rootScope.bEmulator;
        self.lastNotes = [];
        self.lastNotesOfClass = [];
        updateData();
    }

    //Start
    init();
}

angular.module('dashboard')
    .component('dashboard', {
        templateUrl: 'app/dashboard/dashboard.template.html',
        controller: dashboardCtrl
    });