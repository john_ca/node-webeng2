angular.module('core')
    .filter('searchNames', function () {
        return function (data, pSearch) {
            if (!pSearch)return data;
            pSearch = pSearch.toLowerCase();
            var both, sn, pn, output = [];
            angular.forEach(data, function (item) {
                sn = item.surname.toLowerCase();
                pn = item.prename.toLowerCase();
                both = pn + " " + sn;
                if ((sn.indexOf(pSearch) != -1 ) || ( pn.indexOf(pSearch) != -1) || ( both.indexOf(pSearch) != -1)) {
                    output.push(item);
                }
            });
            return output;
        }
    });