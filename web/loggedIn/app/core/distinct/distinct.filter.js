angular.module('core')
    .filter('distinct', function () {               //Filter, der doppelte Werte überspringt
        return function (data, keyname) {
            var output = [],                        //letztendliche Ausgabe
                keys = [];                          //einmalige keys
            angular.forEach(data, function (item) {
                var key = item[keyname];
                if (keys.indexOf(key) === -1) {     //Wenn noch nicht vorhanden
                    keys.push(key);
                    output.push(item);
                }
            });
            return output;
        }
    });