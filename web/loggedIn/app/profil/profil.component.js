/**
 * Created by jonat on 04.07.2016.
 */
function profilCtrl($rootScope, $scope) {
    var self = this;
    var bE;

    self.changePassword = function (pSettings) {
        if (pSettings.newPassword) {
            if (pSettings.newPassword !== pSettings.secPassword) alert("Neue Passwörter stimmen nicht überein!");
            else {
                bE.post("updatePassword", pSettings)
                    .then(function (res) {
                        if (res.correct) {
                            self.settings = {};
                            alert("Passwort erfolgreich geändert!");
                        }
                        else alert("Altes Passwort ist nicht korrekt!");
                    })
            }
        } else alert("Bitte ein neues Passwort eingeben!");

    };
    self.changeEmail = function (email) {
        bE.post("updateEmail", {email: email})
            .then(function () {
                //TODO Success snippet?
                updateData();
            });
    };

    function updateData() {
        bE.get("getOwnEmail")
            .then(function (res) {
                self.email = res.email;
                $scope.$digest();
            });
    }

    function init() {
        bE = $rootScope.bEmulator;
        self.settings = {};
        self.newEmail = "";
        self.email = "";
        updateData();
    }

    init();
}
angular.module('profil')
    .component('profil', {
        templateUrl: 'app/profil/profil.template.html',
        controller: profilCtrl
    });