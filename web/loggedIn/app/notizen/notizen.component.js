function notizenCtrl($rootScope, $scope) {
    var self = this;
    var bE;

    //TODO Generic, probably service
    function switchEditPopUp(p) {
        if (typeof p !== 'undefined') self.showEditPopUp = p;
        else self.showEditPopUp = !self.showEditPopUp;
    }

    //TODO Generic, probably service
    function switchButtonForm(p) {
        if (typeof p !== 'undefined') self.showButtonContent = p;
        else self.showButtonContent = !self.showButtonContent;
    }

    function init() {
        bE = $rootScope.bEmulator;
        self.search = {};
        self.edit_note = {};
        self.new_note = {};
        self.showButtonContent = false;
        self.showEditPopUp = false;
        self.sortType = 'category';                                                                                     // Defaultorder
        self.sortReverse = false;                                                                                       // Defaultsortdirection
        self.Notes = [];
        self.Categories = [];
        self.Students = [];
        updateData();
    }

    function updateData() {
        bE.get("getDisplayableNotes")
            .then(function (res) {
                self.Notes = res;
                $scope.$digest();
            });
        bE.get("getCat_Notes")
            .then(function (res) {
                self.Categories = res;
                $scope.$digest();
            });
        bE.get("getDisplayableStudents")
            .then(function (res) {
                self.Students = res;
                $scope.$digest();
            });
    }

    self.edit = {
        show: function (pNote) {
            self.edit_note = angular.copy(pNote);                                                                       // Delete all
            switchEditPopUp(true);
        },
        excuse: function (pNote) {
            bE.post("excuseNote", pNote)
                .then(function () {
                    //TODO Success snippet?
                    updateData();
                });
        },
        save: function (pEdited) {
            pEdited.excused = !!pEdited.excused;
            bE.post("updateNote", pEdited)
                .then(function () {
                    //TODO Success snippet?
                    updateData();
                });
            switchEditPopUp(false);
        },
        close: function () {
            switchEditPopUp(false);
        }
    };

    self.new = {
        switch: function () {
            switchButtonForm();
        },
        save: function (pNew) {
            pNew.excused = !!pNew.excused;
            bE.post("newNote", pNew)
                .then(function () {
                    //TODO Success snippet?
                    self.new_note = {};
                    updateData();
                });
            switchButtonForm(false);
        },
        close: function () {
            switchButtonForm(false);
        }
    };

    self.delete = function (pNote) {
        if (confirm("Sind Sie sicher, dass Sie die Notiz von " + pNote.teacher + " löschen möchten?")) {
            bE.post("deleteNote", pNote)
                .then(function () {
                    //TODO Success snippet?
                    updateData();
                });
        }
    };

    self.sortTable = function (pType) {                                                                                 //Change Sorting
        self.sortType = pType;
        self.sortReverse = !self.sortReverse;
    };

    //Start
    init();
}
angular.module('notizen')
    .component('notizen', {
        templateUrl: 'app/notizen/notizen.template.html',
        controller: notizenCtrl
    });