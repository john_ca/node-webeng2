function schuelerCtrl($rootScope, $scope) {
    var self = this;
    var bE;

    function init() {
        bE = $rootScope.bEmulator;
        self.searchName = "";
        self.search = {};
        self.edit_student = {};
        self.new_student = {};
        self.new_note = {};
        self.showAddNotePopUp = false;
        self.showButtonContent = false;
        self.showEditPopUp = false;
        self.sortType = 'class';                                // Defaultsortdirection                                                                            // Defaultorder
        self.sortReverse = false;
        self.d_students = [];
        self.classes = [];
        self.categories = [];
        updateData();
    }

    function updateData() {
        bE.get("getDisplayableStudents")
            .then(function (res) {
                self.d_students = res;
                $scope.$digest();
            });
        bE.get("getClasses")
            .then(function (res) {
                self.classes = res;
                $scope.$digest();
            });
        bE.get("getCat_Notes")
            .then(function (res) {
                self.categories = res;
                $scope.$digest();
            });
    }

    function switchEditPopUp(p) {
        if (typeof p !== 'undefined') self.showEditPopUp = p;
        else self.showEditPopUp = !self.showEditPopUp;
    }

    function switchButtonForm(p) {
        if (typeof p !== 'undefined') self.showButtonContent = p;
        else self.showButtonContent = !self.showButtonContent;
    }

    function switchAddNotePopUp(p) {
        if (typeof p !== 'undefined') self.showAddNotePopUp = p;
        else self.showAddNotePopUp = !self.showAddNotePopUp;
    }

    self.newNote = {
        show: function (pStudent) {
            self.new_note = {};
            self.new_note.f_student = pStudent.id;
            self.new_note.student = pStudent.prename + " " + pStudent.surname;
            switchAddNotePopUp(true);
        },
        save: function (pNew) {
            self.new_note.excused = !!self.new_note.excused;
            bE.post("newNote", pNew)
                .then(function () {
                    //TODO Success snippet?
                    self.new_note = {};
                    updateData();
                });
            switchAddNotePopUp(false);
        },
        close: function () {
            switchAddNotePopUp(false);
        }
    };

    self.edit = {
        show: function (pStudent) {
            self.edit_student = angular.copy(pStudent);                                                                 // Delete all References
            console.log(self.edit_student.f_class);
            console.log(self.classes);
            switchEditPopUp(true);
        },
        save: function (pEdited) {
            console.log("here");
            bE.post("updateStudent", pEdited)
                .then(function () {
                    //TODO Success Snippet?
                    updateData();
                });
            switchEditPopUp(false);
        },
        close: function () {
            switchEditPopUp(false);
        }
    };

    self.new = {
        switch: function () {
            switchButtonForm();
        },
        save: function (pNew) {
            bE.post("newStudent", pNew)
                .then(function () {
                    //TODO Success Snippet?
                    self.new_student = {};
                    updateData();
                });
            switchButtonForm(false);
        },
        close: function () {
            switchButtonForm(false);
        }
    };

    self.delete = function (pStudent) {
        if (confirm("Sind Sie sicher, dass Sie den Schüler " + pStudent.prename + " " + pStudent.surname + " löschen möchten?")) {
            bE.post("deleteStudent", pStudent)
                .then(function () {
                    //TODO Success Snippet?
                    updateData();
                });
        }
    };

    self.sortTable = function (pType) {                                                                                 //Change Sorting
        self.sortType = pType;
        self.sortReverse = !self.sortReverse;
    };

    init();
}
angular.module('schueler')
    .component('schueler', {
        templateUrl: 'app/schueler/schueler.template.html',
        controller: schuelerCtrl
    });