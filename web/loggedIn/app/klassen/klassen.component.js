/**
 * Created by jonat on 04.07.2016.
 */
function klassenCtrl($rootScope, $scope) {
    var self = this;
    var bE;

    function init() {
        bE = $rootScope.bEmulator;
        self.search = {};
        self.edit_class = {};
        self.new_class = {};
        self.showButtonContent = false;
        self.showEditPopUp = false;
        self.sortType = 'level';                                                                                        // Defaultorder
        self.sortReverse = false;                                                                                       // Defaultsortdirection
        self.teachers = [];
        self.d_Classes = [];
        updateData();
    }

    function updateData() {
        bE.get("getDisplayableClasses")
            .then(function (res) {
                self.d_Classes = res;
                $scope.$digest();
            });
        bE.get("getTeachers")
            .then(function (res) {
                self.teachers = res;
                $scope.$digest();
            });
    }

    //TODO Generic, probably service
    function switchEditPopUp(p) {
        if (typeof p !== 'undefined') self.showEditPopUp = p;
        else self.showEditPopUp = !self.showEditPopUp;
    }

    //TODO Generic, probably service
    function switchButtonForm(p) {
        if (typeof p !== 'undefined') self.showButtonContent = p;
        else self.showButtonContent = !self.showButtonContent;
    }

    //TODO Generic, Service?
    self.sortTable = function (pType) {                                                                                 //Change Sorting
        self.sortType = pType;
        self.sortReverse = !self.sortReverse;
    };

    self.edit = {
        show: function (pClass) {
            self.edit_class = angular.copy(pClass);                                                                    // Delete all References
            switchEditPopUp(true);
        },
        save: function (pEdited) {
            bE.post("updateClass", pEdited)
                .then(function () {
                    //TODO Success snippet?
                    self.edit_class = {};
                    updateData();
                });
            switchEditPopUp(false);
        },
        close: function () {
            switchEditPopUp(false);
        }
    };

    self.new = {
        switch: function () {
            switchButtonForm();
        },
        save: function (pNew) {
            bE.post("newClass", pNew)
                .then(function () {
                    //TODO Success snippet?
                    self.new_class = {};
                    updateData();
                });
            switchButtonForm(false);
        },
        close: function () {
            switchButtonForm(false);
        }
    };

    self.delete = function (pClass) {
        if (confirm("Sind Sie sicher, dass Sie Klasse " + pClass.level + pClass.classNR + " löschen möchten?")) {
            bE.post("deleteClass", pClass)
                .then(function () {
                    //TODO Success snippet?
                    updateData();
                });
        }
    };

    //Start
    init();
}
angular.module('klassen')
    .component('klassen', {
        templateUrl: 'app/klassen/klassen.template.html',
        controller: klassenCtrl
    });