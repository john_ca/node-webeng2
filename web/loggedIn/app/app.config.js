angular.module('myApp')
    .config(['$locationProvider', '$routeProvider',                                                                     //configures the angular router
        function config($locationProvider, $routeProvider) {
            $locationProvider.hashPrefix('!');
            $routeProvider
                .when('/dashboard', {
                    template: '<dashboard></dashboard>'
                })
                .when('/klassen', {
                    template: '<klassen></klassen>'
                })
                .when('/schueler', {
                    template: '<schueler></schueler>'
                })
                .when('/lehrer', {
                    template: '<lehrer></lehrer>'
                })
                .when('/notizen', {
                    template: '<notizen></notizen>'
                })
                .when('/profil', {
                    template: '<profil></profil>'
                })
                .otherwise('/dashboard');
        }
    ]);