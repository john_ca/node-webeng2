function lehrerCtrl($rootScope, $scope) {
    var self = this;
    var bE;

    function updateData() {
        bE.get("getDisplayableTeacher")
            .then(function (res) {
                self.teachers = res;
                $scope.$digest();
            });
        bE.get("getOwnRights")
            .then(function (res) {
                self.admin = res.admin;
                $scope.$digest();
            });
    }

    function init() {
        bE = $rootScope.bEmulator;
        self.admin = 0;
        self.teachers = [];
        self.search = {};
        self.edit_teacher = {};
        self.new_teacher = {};
        self.showButtonContent = false;
        self.showEditPopUp = false;
        self.sortType = 'surname';                                                                                      // Defaultorder
        self.sortReverse = false;                                                                                       // Defaultsortdirection
        updateData();
    }

    //TODO Generic, probably service
    function switchEditPopUp(p) {
        if (typeof p !== 'undefined') self.showEditPopUp = p;
        else self.showEditPopUp = !self.showEditPopUp;
    }

    //TODO Generic, probably service
    function switchButtonForm(p) {
        if (typeof p !== 'undefined') self.showButtonContent = p;
        else self.showButtonContent = !self.showButtonContent;
    }

    self.edit = {
        show: function (pTeacher) {
            self.edit_teacher = JSON.parse(angular.toJson(pTeacher));                                                 // Delete all References
            switchEditPopUp(true);
        },
        save: function (pEdited) {
            bE.post("updateTeacher", pEdited)
                .then(function (res) {
                    //TODO Success snippet?
                    updateData();
                });
            switchEditPopUp(false);
        },
        close: function () {
            switchEditPopUp(false);
        }
    };

    self.new = {
        switch: function () {
            switchButtonForm();
        },
        save: function (pNew) {
            bE.post("newTeacher", pNew)
                .then(function () {
                    //TODO Success snippet?
                    self.new_teacher = {};
                    updateData();
                });
            switchButtonForm(false);
        },
        close: function () {
            switchButtonForm(false);
        }
    };

    self.delete = function (pTeacher) {
        if (confirm("Sind Sie sicher, dass Sie den Lehrer " + pTeacher.prename + " " + pTeacher.surname + " löschen möchten?")) {
            bE.post("deleteTeacher", pTeacher)
                .then(function () {
                    //TODO Success snippet?
                    updateData();
                });
        }
    };

    //TODO Generic, Service?
    self.sortTable = function (pType) {                                                                                 //Change Sorting
        self.sortType = pType;
        self.sortReverse = !self.sortReverse;
    };

    //Start
    init();
}
angular.module('lehrer')
    .component('lehrer', {
        templateUrl: 'app/lehrer/lehrer.template.html',
        controller: lehrerCtrl
    });