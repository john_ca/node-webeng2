angular.module('loginApp')
    .config(['$locationProvider', '$routeProvider',                                                                     //configures the angular router
        function config($locationProvider, $routeProvider) {
            $locationProvider.hashPrefix('!');
            $routeProvider
                .when('/', {
                    template: '<login></login>'
                })
                .when('/register', {
                    template: '<register></register>'
                })
                .otherwise('/');
        }
    ]);