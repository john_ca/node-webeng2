function registerCtrl($rootScope) {
    var self = this;
    var bS = $rootScope.backendService;

    self.register = function (data) {
        self.message = [];
        if (!data.schoolname) {
            self.message.push("Bitte einen Schulnamen eingeben!");
        }
        if (!data.schooladdress) {
            self.message.push("Bitte eine Schuladress eingeben!");
        }
        if (!data.adminprename) {
            self.message.push("Bitte einen Vornamen eingeben!");
        }
        if (!data.adminsurname) {
            self.message.push("Bitte eine Nachnamen eingeben!");
        }
        if (!data.adminusername) {
            self.message.push("Bitte eine Username eingeben!");
        }
        if (!data.adminemail) {
            self.message.push("Bitte eine E-Mailadresse eingeben!");
        }
        if (!data.adminpassword) {
            self.message.push("Bitte ein Password eingeben!");
        }
        if (!self.message.length) bS.post("newSchool", data)
            .then(function () {
                window.location = "https://node-klassenbuch-162311.appspot.com/";
            });
    };

    function init() {
        self.data = {};
        self.message = [];
    }

    init();
}

angular.module('register')
    .component('register', {
        templateUrl: 'app/register/register.template.html',
        controller: registerCtrl
    });