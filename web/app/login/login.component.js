function loginCtrl($rootScope, $scope) {
    var self = this;
    var bS = $rootScope.backendService;

    self.tryLogin = function (p) {
        self.message = [];
        if (!p.f_school) {
            self.message.push("Bitte eine Schule auswählen!");
        }
        if (!p.username) {
            self.message.push("Bitte einen Usernamen eingeben!");
        }
        if (!p.password) {
            self.message.push("Bitte ein Passwort eingeben!");
        }
        if (!self.message.length)
            bS.post("login", p)
                .then(function (res) {
                    if (res.error == true) {
                        self.message = ['Falscher Nutzernamen oder Passwort!'];
                        $scope.$digest();
                    } else {
                        window.location="/loggedIn";
                    }
                });
    };

    function update() {
        bS.get("getSchools")
            .then(function (res) {
                self.schools = res;
                $scope.$digest();
            })
    }

    function init() {
        self.message = [];
        self.schools = [];
        self.login = {
            username: "",
            password: "",
            f_school: ""
        };
        update();
    }

    init();
}

angular.module('login')
    .component('login', {
        templateUrl: 'app/login/login.template.html',
        controller: loginCtrl
    });