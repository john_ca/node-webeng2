"use strict";
const teacher_1 = require("./server/routes/teacher");
const school_1 = require("./server/routes/school");
const noteCategory_1 = require("./server/routes/noteCategory");
const student_1 = require("./server/routes/student");
const login_1 = require("./server/routes/login");
const note_1 = require("./server/routes/note");
const schoolClass_1 = require("./server/routes/schoolClass");
const restricted_1 = require("./server/routes/restricted");
const redirect_1 = require("./server/routes/redirect");
const google_1 = require("./server/routes/google");
const cookieSession = require("cookie-session");
const express = require('express');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const app = express();
app.enable('trust proxy');
const router = express.Router();
/*      Cookies     */
app.use(cookieParser());
var passport = require('passport');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
/*      GOOGLE Authentification      */
passport.use(new GoogleStrategy({
    clientID: '978304813164-2lh62f2l2p507ofr9en9cggo6pr8hvqc.apps.googleusercontent.com',
    clientSecret: 'DGQ-8GUV1AKWSI8e84tfp84v',
    callbackURL: "https://node-klassenbuch.appspot.com/auth/google/callback"
}, function (accessToken, refreshToken, profile, done) {
    let email = profile.emails[0].value;
    return done(null, profile.id);
}));
passport.serializeUser(function (user, done) {
    done(null, user);
});
passport.deserializeUser(function (user, done) {
    done(null, user);
});
app.use(passport.initialize());
app.use(passport.session());
/*      Session     */
app.use(cookieSession({
    name: 'session',
    secret: 'the answer to life the universe and everything',
    keys: ['test']
}));
/*      Body Parser     */
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
/*      Setup Routes        */
restricted_1.default(app);
redirect_1.default(app);
google_1.default(app);
teacher_1.default(app);
school_1.default(app);
noteCategory_1.default(app);
note_1.default(app);
login_1.default(app);
schoolClass_1.default(app);
student_1.default(app);
/*      Static Files        */
app.use(express.static('web'));
/*      Default Route       */
app.get('/', (req, res) => {
    res.sendFile(`${__dirname}/web/index.html`);
});
/*      Server Start        */
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`App listening on port ${PORT}`);
});
//# sourceMappingURL=server.js.map