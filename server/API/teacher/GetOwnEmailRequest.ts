export class GetOwnEmailResponseObject {
    email: string;

    constructor(email: string) {
        this.email = email;
    }
}