export class NewTeacherRequest {
    prename: string;
    surname: string;
    username: string;
    email: string;
    admin: boolean;

    constructor(prename: string, surname: string, username: string, email: string, admin: boolean, public password: string) {
        this.prename = prename;
        this.surname = surname;
        this.username = username;
        this.email = email;
        this.admin = admin;
    }
}

export const NewTeacherRequestStructure = {
    prename: "string",
    surname: "string",
    username: "string",
    email: "string",
    password: "string"
};