export class UpdateEmailRequest {
    email: string;

    constructor(email: string) {
        this.email = email;
    }
}

export const UpdateEmailRequestStructure = {
    email: "string"
};