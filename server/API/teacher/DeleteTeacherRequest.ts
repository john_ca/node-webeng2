export class DeleteTeacherRequest{
    id: string;

    constructor(id: string) {
        this.id = id;
    }
}

export const DeleteTeacherRequestStructure = {
    id: "string"
};