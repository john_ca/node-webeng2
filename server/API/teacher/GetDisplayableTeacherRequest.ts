class GetDisplayableTeacherResponseObject {
    id: string;
    prename: string;
    surname: string;
    username: string;
    email: string;
    admin: boolean;
    rolename: string;

    constructor(id: string, prename: string, surname: string, username: string, email: string, admin: boolean, rolename: string) {
        this.id = id;
        this.prename = prename;
        this.surname = surname;
        this.username = username;
        this.email = email;
        this.admin = admin;
        this.rolename = rolename;
    }
}

export class GetDisplayableTeacherResponse extends Array<GetDisplayableTeacherResponseObject> {
}