export class GetOwnRightsResponseObject {
    admin: boolean;

    constructor(admin: boolean) {
        this.admin = admin;
    }
}