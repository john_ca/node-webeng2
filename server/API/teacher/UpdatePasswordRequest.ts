export class UpdatePasswordRequest {
    password: string;
    newPassword: string;

    constructor(password: string, newPassword: string) {
        this.password = password;
        this.newPassword = newPassword;
    }
}

export class UpdatePasswordResponse {
    correct: boolean;

    constructor(correct: boolean) {
        this.correct = correct;
    }
}

export const UpdatePasswordRequestStructure = {
    password: "string",
    newPassword: "string",
};