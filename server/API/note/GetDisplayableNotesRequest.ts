class GetDisplayableNotesResponseObject {
    id: string;
    f_teacher: number;
    f_student: number;
    f_category: number;
    text: string;
    excused: boolean;
    teacher: string;
    student: string;
    category: string;

    constructor(id: string, f_teacher: number, f_student: number, f_category: number, text: string, excused: boolean, teacher: string, student: string, category: string) {
        this.id = id;
        this.f_teacher = f_teacher;
        this.f_student = f_student;
        this.f_category = f_category;
        this.text = text;
        this.excused = excused;
        this.teacher = teacher;
        this.student = student;
        this.category = category;
    }
}

export class GetDisplayableNotesResponse extends Array<GetDisplayableNotesResponseObject> {
}