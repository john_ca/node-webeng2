export class UpdateNoteRequest {
    id: string;
    f_teacher: string;
    f_student: string;
    f_category: string;
    text: string;
    excused: boolean;

    constructor(id: string, f_teacher: string, f_student: string, f_category: string, text: string, excused: boolean, public f_school: any) {
        this.id = id;
        this.f_teacher = f_teacher;
        this.f_student = f_student;
        this.f_category = f_category;
        this.text = text;
        this.excused = excused;
    }
}

export const UpdateNoteRequestStructure = {
    id: "string",
    f_teacher: "string",
    f_student: "string",
    f_category: "string",
    text: "string",
    excused: "boolean"
};