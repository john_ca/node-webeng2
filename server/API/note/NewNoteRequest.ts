export class NewNoteRequest {
    f_student: string;
    f_category: string;
    text: string;
    excused: boolean;

    constructor(f_student: string, f_category: string, text: string, excused: boolean, public f_school: string, public time: string) {
        this.f_student = f_student;
        this.f_category = f_category;
        this.text = text;
        this.excused = excused;
    }
}

export const NewNoteRequestStructure = {
    f_student: "string",
    f_category: "string",
    text: "string",
    excused: "boolean"
};