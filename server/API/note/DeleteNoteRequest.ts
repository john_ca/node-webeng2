export class DeleteNoteRequest {
    id: string;

    constructor(id: string) {
        this.id = id;
    }
}

export const DeleteNoteRequestStructure = {
    id: "string"
};