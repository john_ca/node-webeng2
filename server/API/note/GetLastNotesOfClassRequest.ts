class GetLastNotesOfClassResponseObject {
    id: string;
    student: string;
    category: string;
    text: string;
    excused: boolean;

    constructor(id: string, student: string, category: string, text: string, excused: boolean) {
        this.id = id;
        this.student = student;
        this.category = category;
        this.text = text;
        this.excused = excused;
    }
}

export class GetLastNotesOfClassResponse extends Array<Array<GetLastNotesOfClassResponseObject>> {
}