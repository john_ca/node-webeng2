export class ExcuseNoteRequest {
    id: string;

    constructor(id: string) {
        this.id = id;
    }
}

export const ExcuseNoteRequestStructure = {
    id: "string"
};