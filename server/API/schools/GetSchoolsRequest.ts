export class GetSchoolsResponseObject {
    id: string;
    name: string;

    constructor(id: string, name: string) {
        this.id = id;
        this.name = name;
    }
}

export class GetSchoolsResponse extends Array<GetSchoolsResponseObject> {
}