export class NewSchoolRequest {
    schoolname: string;
    schooladdress: string;
    adminprename: string;
    adminsurname: string;
    adminusername: string;
    adminemail: string;
    adminpassword: string;

    constructor(schoolname: string, schooladdress: string, adminprename: string, adminsurname: string, adminusername: string, adminemail: string, adminpassword: string) {
        this.schoolname = schoolname;
        this.schooladdress = schooladdress;
        this.adminprename = adminprename;
        this.adminsurname = adminsurname;
        this.adminusername = adminusername;
        this.adminemail = adminemail;
        this.adminpassword = adminpassword;
    }
}

export const NewSchoolRequestStructure = {
    schoolname: "string",
    schooladdress: "string",
    adminprename: "string",
    adminsurname: "string",
    adminusername: "string",
    adminemail: "string",
    adminpassword: "string",
};