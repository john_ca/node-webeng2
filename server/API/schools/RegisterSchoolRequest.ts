export class RegisterSchoolRequest {
    id: string;
    name: string;

    constructor(id: string, name: string) {
        this.id = id;
        this.name = name;
    }
}

export const RegisterSchoolRequestStructure = {
    id: "string",
    name: "string"
};