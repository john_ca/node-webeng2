export class UpdateStudentRequest {
    id: string;
    prename: string;
    surname: string;
    f_class: string;

    constructor(id: string, prename: string, surname: string, f_class: string) {
        this.id = id;
        this.prename = prename;
        this.surname = surname;
        this.f_class = f_class;
    }
}

export const UpdateStudentRequestStructure = {
    id: "string",
    prename: "string",
    surname: "string",
    f_class: "string"
};