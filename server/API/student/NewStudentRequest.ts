export class NewStudentRequest {
    prename: string;
    surname: string;
    f_class: string;

    constructor(prename: string, surname: string, f_class: string) {
        this.prename = prename;
        this.surname = surname;
        this.f_class = f_class;
    }
}

export const NewStudentRequestStructure = {
    prename: "string",
    surname: "string",
    f_class: "string"
};