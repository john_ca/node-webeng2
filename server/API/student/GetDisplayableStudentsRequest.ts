export class GetDisplayableStudentsResponseObject {
    id: any;
    prename: string;
    surname: string;
    f_class: string;
    className: string;

    constructor(id: any, prename: string, surname: string, f_class: string, className: string) {
        this.id = id;
        this.prename = prename;
        this.surname = surname;
        this.f_class = f_class;
        this.className = className;
    }
}

export class GetDisplayableStudentsResponse extends Array<GetDisplayableStudentsResponseObject> {
}