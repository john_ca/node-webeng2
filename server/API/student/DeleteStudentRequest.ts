export class DeleteStudentRequest {
    id: string;

    constructor(id: string) {
        this.id = id;
    }
}

export const DeleteStudentRequestStructure = {
    id: "string"
};