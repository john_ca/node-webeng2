export class NewNoteCategoryRequest {
    id: string;
    name: string;

    constructor(id: string, name: string) {
        this.id = id;
        this.name = name;
    }
}

export const NewNoteCategoryRequestStructure = {
    id: "string",
    name: "string"
};