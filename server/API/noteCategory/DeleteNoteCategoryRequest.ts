export class DeleteNoteCategoryRequest{
    id: string;

    constructor(id: string) {
        this.id = id;
    }
}

export const DeleteNoteCategoryRequestStructure = {
    id: "string"
};