export class NewClassRequest {
    level: number;
    classNR: string;
    f_teacher: string;

    constructor(level: number, classNR: string, f_teacher: string) {
        this.level = level;
        this.classNR = classNR;
        this.f_teacher = f_teacher;
    }
}

export const NewClassRequestStructure = {
    level: "number",
    classNR: "string",
    f_teacher: "string",
};