export class GetClassesResponseObject {
    id: string;
    level: number;
    classNR: string;
    f_teacher: number;

    constructor(id: string, level: number, classNR: string, f_teacher: number) {
        this.id = id;
        this.level = level;
        this.classNR = classNR;
        this.f_teacher = f_teacher;
    }
}

export class GetClassesResponse extends Array<GetClassesResponseObject> {
}