export class UpdateClassRequest {
    id: string;
    level: number;
    classNR: string;
    f_teacher: string;

    constructor(id: string, level: number, classNR: string, f_teacher: string) {
        this.id = id;
        this.level = level;
        this.classNR = classNR;
        this.f_teacher = f_teacher;
    }
}

export const UpdateClassRequestStructure = {
    id: "string",
    level: "number",
    classNR: "string",
    f_teacher: "string"
};