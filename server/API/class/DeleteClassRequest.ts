export class DeleteClassRequest {
    id: string;

    constructor(id: string) {
        this.id = id;
    }
}

export const DeleteClassRequestStructure = {
    id: "string"
};