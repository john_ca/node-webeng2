export class GetDisplayableClassesResponseObject {
    id: string;
    level: number;
    classNR: string;
    f_teacher: number;
    teacher: string;

    constructor(id: string, level: number, classNR: string, f_teacher: number, teacher: string) {
        this.id = id;
        this.level = level;
        this.classNR = classNR;
        this.f_teacher = f_teacher;
        this.teacher = teacher;
    }
}

export class GetDisplayableClassesResponse extends Array<GetDisplayableClassesResponseObject> {
}