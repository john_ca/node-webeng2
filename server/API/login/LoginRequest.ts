export class LoginRequest {
    password: string;
    username: string;
    f_school: string;

    constructor(password: string, username: string, f_school: string) {
        this.password = password;
        this.username = username;
        this.f_school = f_school;
    }
}

export class LoginResponse {
    error: boolean;

    constructor(error: boolean) {
        this.error = error;
    }
}

export const LoginRequestStructure = {
    password: "string",
    username: "string",
    f_school: "string"
};