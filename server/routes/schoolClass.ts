import {Response, Request} from "express";
import {UpdateClassRequest, UpdateClassRequestStructure} from "../API/class/UpdateClassRequest";
import {ParserService} from "../services/ParserService";
import {NewClassRequest, NewClassRequestStructure} from "../API/class/NewClassRequest";
import {SchoolClassService} from "../services/school-class.service";
import {DeleteClassRequest, DeleteClassRequestStructure} from "../API/class/DeleteClassRequest";
import {SchoolClass} from "../models/school-class.model";
import {TeacherService} from "../services/teacher.service";
import {Teacher} from "../models/teacher.model";
import {GetDisplayableClassesResponse} from "../API/class/GetDisplayableClassesRequest";
import {GetClassesResponse} from "../API/class/GetClassRequest";

export default function (app) {
    let pS = new ParserService();
    let sS = new SchoolClassService();
    let sT = new TeacherService();

    app.get("/loggedIn/getDisplayableClasses", function (req: Request, res: Response) {
        let schoolKey = req.session['school'];
        sS.getAll(schoolKey).then((classEntries: Array<SchoolClass>) => {
            sT.getAll(schoolKey).then((teacherEntries: Array<Teacher>) => {
                let teacher = [];
                let answer: GetDisplayableClassesResponse = [];
                for (let entry of teacherEntries) teacher[entry.key] = entry;
                for (let klasse of classEntries)
                    answer.push({
                        id: klasse.key,
                        level: klasse.level,
                        classNR: klasse.identifier,
                        f_teacher: klasse.teacherKey,
                        teacher: (teacher[klasse.teacherKey]) ? teacher[klasse.teacherKey].prename + " " + teacher[klasse.teacherKey].surname : "Kein Lehrer"
                    });
                res.json(answer);
            })
        });
    });

    app.get("/loggedIn/getClasses", function (req: Request, res: Response) {
        let schoolKey = req.session['school'];
        sS.getAll(schoolKey).then((classes: Array<SchoolClass>) => {
            let answer: GetClassesResponse = [];
            for (let klasse of classes)
                answer.push({
                    id: klasse.key,
                    level: klasse.level,
                    classNR: klasse.identifier,
                    f_teacher: klasse.teacherKey
                });
            res.json(answer);
        })
    });

    app.post('/loggedIn/updateClass', function (req: Request|any, res: Response) {
        let data: UpdateClassRequest = pS.parse(req.body, UpdateClassRequestStructure);
        if (!data) return res.sendStatus(400);
        sS.update({
            key: data.id,
            level: data.level,
            identifier: data.classNR,
            teacherKey: data.f_teacher,
            schoolKey: req.session["school"]
        }).then(() => {
            res.sendStatus(200);
        });
    });

    app.post('/loggedIn/newClass', function (req, res) {
        let data: NewClassRequest = pS.parse(req.body, NewClassRequestStructure);
        if (!data) return res.sendStatus(400);
        sS.insert({
            key: null,
            level: data.level,
            identifier: data.classNR,
            teacherKey: data.f_teacher,
            schoolKey: req.session["school"]
        }).then(() => {
            res.sendStatus(200);
        });
    });

    app.post('/loggedIn/deleteClass', function (req, res) {
        let data: DeleteClassRequest = pS.parse(req.body, DeleteClassRequestStructure);
        if (!data) return res.sendStatus(400);
        sS.delete(data.id).then(() => {
            res.sendStatus(200);
        });
    });

};