import { TeacherService } from './../services/teacher.service';
var passport = require('passport');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

export default function (app) {
    let tS = new TeacherService();

    app.get('/auth/google', passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/plus.login', 'email'] }));
    app.get('/auth/google/callback', passport.authenticate('google', {  }), function(req, res) {

        if(req.session['user']) {
            tS.get(req.session['user']).then(teacher => {
                teacher.googleId = req.user;

                tS.update(teacher).then(() => {
                    res.redirect("/loggedIn");
                })
            });
        }
        else {
            tS.getAllGoogle().then(teachers => {
                for(let teacher of teachers) {
                    if(teacher.googleId == req.user) {
                        req.session['user'] = teacher.key;
                        req.session['school'] = teacher.schoolKey;

                        res.redirect("/loggedIn");
                    }
                }
                // No user found

                res.redirect("/app/google.html");
            })
        }
    });
}