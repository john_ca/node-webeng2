"use strict";
const ParserService_1 = require('../services/ParserService');
const BCryptService_1 = require("../services/BCryptService");
const LoginRequest_1 = require("../API/login/LoginRequest");
const gcloud = require('google-cloud');
let datastore = gcloud.datastore({
    projectId: 'node-klassenbuch',
    keyFilename: './Node-Klassenbuch-1ffc73dd3e10.json'
});
function default_1(app) {
    let pS = new ParserService_1.ParserService();
    let bS = new BCryptService_1.default();
    app.post('/login', function (req, res, next) {
        let data = pS.parse(req.body, LoginRequest_1.LoginRequestStructure);
        if (!data)
            res.status(400);
        datastore.createQuery('Teacher')
            .filter('username', data.username)
            .run()
            .then((results) => {
            for (let result of results[0]) {
                if (result.schoolKey == data.f_school) {
                    let user = results[0][0];
                    return bS.compare(data.password, user.password).then(result => {
                        if (result) {
                            req.session["user"] = user[datastore.KEY].id;
                            req.session["school"] = user.schoolKey;
                            return res.status(200).json({ error: false });
                        }
                        else {
                            return res.status(200).json({ error: true });
                        }
                    });
                }
            }
            res.status(200).json({ error: true });
        });
    });
    app.post('/logout', function (req, res) {
        req.session = null;
        res.json({ error: false });
    });
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
;
//# sourceMappingURL=login.js.map