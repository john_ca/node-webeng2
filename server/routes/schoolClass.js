"use strict";
const UpdateClassRequest_1 = require("../API/class/UpdateClassRequest");
const ParserService_1 = require("../services/ParserService");
const NewClassRequest_1 = require("../API/class/NewClassRequest");
const school_class_service_1 = require("../services/school-class.service");
const DeleteClassRequest_1 = require("../API/class/DeleteClassRequest");
const teacher_service_1 = require("../services/teacher.service");
function default_1(app) {
    let pS = new ParserService_1.ParserService();
    let sS = new school_class_service_1.SchoolClassService();
    let sT = new teacher_service_1.TeacherService();
    app.get("/loggedIn/getDisplayableClasses", function (req, res) {
        let schoolKey = req.session['school'];
        sS.getAll(schoolKey).then((classEntries) => {
            sT.getAll(schoolKey).then((teacherEntries) => {
                let teacher = [];
                let answer = [];
                for (let entry of teacherEntries)
                    teacher[entry.key] = entry;
                for (let klasse of classEntries)
                    answer.push({
                        id: klasse.key,
                        level: klasse.level,
                        classNR: klasse.identifier,
                        f_teacher: klasse.teacherKey,
                        teacher: (teacher[klasse.teacherKey]) ? teacher[klasse.teacherKey].prename + " " + teacher[klasse.teacherKey].surname : "Kein Lehrer"
                    });
                res.json(answer);
            });
        });
    });
    app.get("/loggedIn/getClasses", function (req, res) {
        let schoolKey = req.session['school'];
        sS.getAll(schoolKey).then((classes) => {
            let answer = [];
            for (let klasse of classes)
                answer.push({
                    id: klasse.key,
                    level: klasse.level,
                    classNR: klasse.identifier,
                    f_teacher: klasse.teacherKey
                });
            res.json(answer);
        });
    });
    app.post('/loggedIn/updateClass', function (req, res) {
        let data = pS.parse(req.body, UpdateClassRequest_1.UpdateClassRequestStructure);
        if (!data)
            return res.sendStatus(400);
        sS.update({
            key: data.id,
            level: data.level,
            identifier: data.classNR,
            teacherKey: data.f_teacher,
            schoolKey: req.session["school"]
        }).then(() => {
            res.sendStatus(200);
        });
    });
    app.post('/loggedIn/newClass', function (req, res) {
        let data = pS.parse(req.body, NewClassRequest_1.NewClassRequestStructure);
        if (!data)
            return res.sendStatus(400);
        sS.insert({
            key: null,
            level: data.level,
            identifier: data.classNR,
            teacherKey: data.f_teacher,
            schoolKey: req.session["school"]
        }).then(() => {
            res.sendStatus(200);
        });
    });
    app.post('/loggedIn/deleteClass', function (req, res) {
        let data = pS.parse(req.body, DeleteClassRequest_1.DeleteClassRequestStructure);
        if (!data)
            return res.sendStatus(400);
        sS.delete(data.id).then(() => {
            res.sendStatus(200);
        });
    });
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
;
//# sourceMappingURL=schoolClass.js.map