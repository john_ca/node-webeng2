import Express = e.Express;
import * as e from "express";
import {Response, Request} from "express";

export default function (app: Express) {
    app.all('/loggedIn*', function (req: Request|any, res: Response, next) {
        if (req.session["user"] && req.session['school']) next();
        else {
            res.sendStatus(401);                                                                                          //Not logged in
        }
    })
}