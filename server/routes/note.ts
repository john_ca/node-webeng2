import {NoteCategory} from '../models/note-category.model';
import {Student} from '../models/student.model';
import {Teacher} from '../models/teacher.model';
import {StudentService} from '../services/student.service';
import {TeacherService} from '../services/teacher.service';
import {GetDisplayableNotesResponse} from '../API/note/GetDisplayableNotesRequest';
import {Response, Request} from "express";
import {ParserService} from "../services/ParserService";
import {UpdateNoteRequestStructure, UpdateNoteRequest} from "../API/note/UpdateNoteRequest";
import {NewNoteRequest, NewNoteRequestStructure} from "../API/note/NewNoteRequest";
import {DeleteNoteRequest, DeleteNoteRequestStructure} from "../API/note/DeleteNoteRequest";
import {ExcuseNoteRequest, ExcuseNoteRequestStructure} from "../API/note/ExcuseNoteRequest";
import {NoteService} from "../services/note.service";
import {NoteCategoryService} from "../services/note-category.service";
import {Note} from "../models/note.model";
import {GetLastNotesResponse} from "../API/note/GetLastNotesRequest";
import {GetLastNotesOfClassResponse} from "../API/note/GetLastNotesOfClassRequest";
import {SchoolClassService} from "../services/school-class.service";
import {SchoolClass} from "../models/school-class.model";

export default function (app) {
    let pS = new ParserService();
    let nS = new NoteService();
    let ncS = new NoteCategoryService();
    let tS = new TeacherService();
    let sS = new StudentService();
    let cS = new SchoolClassService();

    app.post('/loggedIn/updateNote', function (req: Request|any, res: Response) {
        let data: UpdateNoteRequest = pS.parse(req.body, UpdateNoteRequestStructure);
        if (!data) return res.sendStatus(400);
        nS.update({
            key: data.id,
            teacherKey: data.f_teacher,
            studentKey: data.f_student,
            noteCategoryKey: data.f_category,
            time: new Date().getTime(),
            text: data.text,
            excused: data.excused,
            schoolKey: req.session["school"]
        }).then(() => {
            res.sendStatus(200);
        });
    });

    app.post('/loggedIn/newNote', function (req: Request|any, res: Response) {
        let data: NewNoteRequest = pS.parse(req.body, NewNoteRequestStructure);
        if (!data) return res.sendStatus(400);
        nS.insert({
            key: null,
            teacherKey: req.session["user"],
            studentKey: data.f_student,
            noteCategoryKey: data.f_category,
            time: new Date().getTime(),
            text: data.text,
            excused: data.excused,
            schoolKey: req.session["school"]
        }).then(() => {
            res.sendStatus(200);
        });
    });

    app.post('/loggedIn/deleteNote', function (req: Request, res: Response) {
        let data: DeleteNoteRequest = pS.parse(req.body, DeleteNoteRequestStructure);
        if (!data) return res.sendStatus(400);
        nS.delete(data.id).then(() => {
            res.sendStatus(200);
        });
    });

    app.post('/loggedIn/excuseNote', function (req: Request, res: Response) {
        let data: ExcuseNoteRequest = pS.parse(req.body, ExcuseNoteRequestStructure);
        if (!data) return res.sendStatus(400);
        nS.get(data.id)
            .then((note: Note) => {
                note.excused = !note.excused;
                return nS.update(note)
                    .then(() => {
                        res.sendStatus(200);
                    })
            });
    });

    app.get('/loggedIn/getLastNotes', function (req: Request, res: Response) {
        let answers: GetLastNotesResponse = [];
        let teacherKey = req.session["user"];
        let schoolKey = req.session["school"];
        return sS.getAll(schoolKey).then((studentEntries: Student[]) => {
            return ncS.getAll().then((categoryEntries: NoteCategory[]) => {
                let students = {};
                let categories = {};
                for (let category of categoryEntries) categories[category.key] = category;
                for (let student of studentEntries) students[student.key] = student;
                return nS.getAll(schoolKey)
                    .then((notes: Note[]) => {
                        notes = notes.filter(val => (val.teacherKey == teacherKey));
                        notes = notes.slice(0, 5);
                        answers = notes.map(note => {
                            return {
                                id: note.key,
                                text: note.text,
                                excused: note.excused,
                                student: (students[note.studentKey]) ? students[note.studentKey].prename + " " + students[note.studentKey].surname : "Kein Schüler",
                                category: (categories[note.noteCategoryKey]) ? categories[note.noteCategoryKey].name : "Keine Kategorie"
                            }
                        });
                        return res.json(answers);
                    });
            })
        })
    });

    app.get('/loggedIn/getLastNotesOfClass', function (req: Request, res: Response) {
        let answers: GetLastNotesOfClassResponse|any = {};
        let teacherKey = req.session["user"];
        let schoolKey = req.session["school"];
        return sS.getAll(schoolKey).then((studentEntries: Student[]) => {
            return ncS.getAll().then((categoryEntries: NoteCategory[]) => {
                return cS.getAll(schoolKey).then((classEntries: SchoolClass[]) => {
                    let students = {};
                    let classes = {};
                    let categories = {};
                    for (let schoolClass of classEntries)classes[schoolClass.key] = schoolClass;
                    for (let category of categoryEntries) categories[category.key] = category;
                    for (let student of studentEntries) students[student.key] = student;
                    return nS.getAll(schoolKey)
                        .then((notes: Note[]) => {
                            for (let note of notes) {
                                try {
                                    let sClass = classes[students[note.studentKey].classKey];
                                    if (sClass.teacherKey == teacherKey)   //Check if teacher of class of student of note is classteacher
                                    {
                                        let className = sClass.level + '' + sClass.identifier;  //Compose Classname
                                        let elem = {
                                            id: note.key,
                                            text: note.text,
                                            excused: note.excused,
                                            student: (students[note.studentKey]) ? students[note.studentKey].prename + " " + students[note.studentKey].surname : "Kein Schüler",
                                            category: (categories[note.noteCategoryKey]) ? categories[note.noteCategoryKey].name : "Keine Kategorie"
                                        };
                                        answers[className] ? answers[className].push(elem) : answers[className] = [elem];  //Create array in array, if not already existing
                                    }
                                } catch (e) {
                                }
                            }
                            res.json(answers);
                        });
                });
            });
        })
    });

    app.get('/loggedIn/getDisplayableNotes', function (req, res) {
        let answers: GetDisplayableNotesResponse = [];
        let schoolKey = req.session["school"];
        tS.getAll(schoolKey).then((teacherEntries: Teacher[]) => {
            return sS.getAll(schoolKey).then((studentEntries: Student[]) => {
                return ncS.getAll().then((categoryEntries: NoteCategory[]) => {
                    let students = [];
                    let teachers = [];
                    let categories = [];
                    for (let category of categoryEntries) categories[category.key] = category;
                    for (let student of studentEntries) students[student.key] = student;
                    for (let teacher of teacherEntries) teachers[teacher.key] = teacher;
                    return nS.getAll(schoolKey)
                        .then((notes: Note[]) => {
                            for (let note of notes)
                                answers.push({
                                    id: note.key,
                                    f_teacher: note.teacherKey,
                                    f_student: note.studentKey,
                                    f_category: note.noteCategoryKey,
                                    text: note.text,
                                    excused: note.excused,
                                    teacher: (teachers[note.teacherKey]) ? teachers[note.teacherKey].prename + " " + teachers[note.teacherKey].surname : "Kein Lehrer",
                                    student: (students[note.studentKey]) ? students[note.studentKey].prename + " " + students[note.studentKey].surname : "Kein Schüler",
                                    category: (categories[note.noteCategoryKey]) ? categories[note.noteCategoryKey].name : "Keine Kategorie"
                                })
                            return res.json(answers);
                        })
                })
            })
        })
    });
}