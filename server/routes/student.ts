import {SchoolClassService} from '../services/school-class.service';
import {StudentService} from '../services/student.service';
import {Response, Request} from "express";
import {UpdateStudentRequest, UpdateStudentRequestStructure} from "../API/student/UpdateStudentRequest";
import {ParserService} from "../services/ParserService";
import {NewStudentRequest, NewStudentRequestStructure} from "../API/student/NewStudentRequest";
import {DeleteStudentRequest} from "../API/student/DeleteStudentRequest";
import {DeleteClassRequestStructure} from "../API/class/DeleteClassRequest";
import {Student} from "../models/student.model";
import {GetDisplayableStudentsResponse} from "../API/student/GetDisplayableStudentsRequest";
import {SchoolClass} from "../models/school-class.model";


export default function (app) {
    let pS = new ParserService();
    let sS = new StudentService();
    let cS = new SchoolClassService();

    /* GET */
    app.get('/loggedIn/getDisplayableStudents', function (req, res) {
        let schoolKey = req.session["school"];
        sS.getAll(schoolKey)
            .then((students: Student[]) => {
                cS.getAll(schoolKey).then((classEntries: SchoolClass[]) => {
                    let answers: GetDisplayableStudentsResponse = [];
                    let classes = [];
                    for (let entry of classEntries) classes[entry.key] = entry;
                    for (let student of students)
                        answers.push({
                            id: student.key,
                            prename: student.prename,
                            surname: student.surname,
                            f_class: student.classKey,
                            className: (classes[student.classKey]) ? classes[student.classKey].level + "" + classes[student.classKey].identifier : "Keine Klasse!",
                        });
                    res.json(answers);
                });
            });
    });

    /* POST */
    app.post('/loggedIn/updateStudent', function (req: Request|any, res: Response) {
        let data: UpdateStudentRequest = pS.parse(req.body, UpdateStudentRequestStructure);
        if (!data) return res.sendStatus(400);
        sS.update({
            key: data.id,
            prename: data.prename,
            surname: data.surname,
            classKey: data.f_class,
            schoolKey: req.session["school"]
        }).then(() => {
            res.sendStatus(200);
        });
    });

    app.post('/loggedIn/newStudent', function (req: Request|any, res: Response) {
        let data: NewStudentRequest = pS.parse(req.body, NewStudentRequestStructure);
        if (!data) return res.sendStatus(400);
        sS.insert({
            key: null,
            prename: data.prename,
            surname: data.surname,
            classKey: data.f_class,
            schoolKey: req.session["school"]
        }).then(() => {
            res.sendStatus(200);
        });
    });

    app.post('/loggedIn/deleteStudent', function (req: Request, res: Response) {
        let data: DeleteStudentRequest = pS.parse(req.body, DeleteClassRequestStructure);
        if (!data) return res.sendStatus(400);
        sS.delete(data.id).then(() => {
            res.sendStatus(200);
        });
    });
}