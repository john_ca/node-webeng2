"use strict";
const school_class_service_1 = require('../services/school-class.service');
const student_service_1 = require('../services/student.service');
const UpdateStudentRequest_1 = require("../API/student/UpdateStudentRequest");
const ParserService_1 = require("../services/ParserService");
const NewStudentRequest_1 = require("../API/student/NewStudentRequest");
const DeleteClassRequest_1 = require("../API/class/DeleteClassRequest");
function default_1(app) {
    let pS = new ParserService_1.ParserService();
    let sS = new student_service_1.StudentService();
    let cS = new school_class_service_1.SchoolClassService();
    /* GET */
    app.get('/loggedIn/getDisplayableStudents', function (req, res) {
        let schoolKey = req.session["school"];
        sS.getAll(schoolKey)
            .then((students) => {
            cS.getAll(schoolKey).then((classEntries) => {
                let answers = [];
                let classes = [];
                for (let entry of classEntries)
                    classes[entry.key] = entry;
                for (let student of students)
                    answers.push({
                        id: student.key,
                        prename: student.prename,
                        surname: student.surname,
                        f_class: student.classKey,
                        className: (classes[student.classKey]) ? classes[student.classKey].level + "" + classes[student.classKey].identifier : "Keine Klasse!",
                    });
                res.json(answers);
            });
        });
    });
    /* POST */
    app.post('/loggedIn/updateStudent', function (req, res) {
        let data = pS.parse(req.body, UpdateStudentRequest_1.UpdateStudentRequestStructure);
        if (!data)
            return res.sendStatus(400);
        sS.update({
            key: data.id,
            prename: data.prename,
            surname: data.surname,
            classKey: data.f_class,
            schoolKey: req.session["school"]
        }).then(() => {
            res.sendStatus(200);
        });
    });
    app.post('/loggedIn/newStudent', function (req, res) {
        let data = pS.parse(req.body, NewStudentRequest_1.NewStudentRequestStructure);
        if (!data)
            return res.sendStatus(400);
        sS.insert({
            key: null,
            prename: data.prename,
            surname: data.surname,
            classKey: data.f_class,
            schoolKey: req.session["school"]
        }).then(() => {
            res.sendStatus(200);
        });
    });
    app.post('/loggedIn/deleteStudent', function (req, res) {
        let data = pS.parse(req.body, DeleteClassRequest_1.DeleteClassRequestStructure);
        if (!data)
            return res.sendStatus(400);
        sS.delete(data.id).then(() => {
            res.sendStatus(200);
        });
    });
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
//# sourceMappingURL=student.js.map