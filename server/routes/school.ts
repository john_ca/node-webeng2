import { ParserService } from './../services/ParserService';
import { RegisterSchoolRequest, RegisterSchoolRequestStructure } from './../API/schools/RegisterSchoolRequest';
import { GetSchoolsResponse, GetSchoolsResponseObject } from './../API/schools/GetSchoolsRequest';
import { SchoolService } from './../services/school.service';
import {Response, Request} from "express";
import {NewSchoolRequest, NewSchoolRequestStructure} from "./../API/schools/NewSchoolRequest";
import {TeacherService} from "../services/teacher.service";
import BCryptService from "../services/BCryptService";


export default function (app) {
    let pS = new ParserService();
    let sS = new SchoolService();
    let tS = new TeacherService();
    let bC = new BCryptService();

    /*      GET     */
    app.get('/getSchools', function (req: Request, res: Response) {
        sS.getAll().then(schools => {
            let responseObjects: GetSchoolsResponse = [];
            for (let school of schools) {
                responseObjects.push(new GetSchoolsResponseObject(school.key, school.name));
            }
            res.json(responseObjects);
        });
    });

    /*      POST        */
    app.post('/newSchool', function (req: Request, res: Response) {
        let data: NewSchoolRequest = pS.parse(req.body, NewSchoolRequestStructure);
        if (!data) return res.sendStatus(400);
        
        sS.insert({
            key: null,
            name: data.schoolname,
            address: data.schooladdress
        })
        .then((school: School) => {
            return bC.hash(data.adminpassword)
                .then(hash => {
                    return tS.insert({
                        key: null,
                        prename: data.adminprename,
                        surname: data.adminsurname,
                        username: data.adminusername,
                        email: data.adminemail,
                        admin: true,
                        password: hash,
                        schoolKey: school.key
                    })
                        .then(() => {
                            res.sendStatus(200);
                        })
                })

        })
        res.redirect('/');
    });
};
