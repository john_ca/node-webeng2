"use strict";
const note_category_service_1 = require('./../services/note-category.service');
const note_category_model_1 = require('./../models/note-category.model');
const NewNoteCategoryRequest_1 = require("../API/noteCategory/NewNoteCategoryRequest");
const DeleteNoteCategoryRequest_1 = require("../API/noteCategory/DeleteNoteCategoryRequest");
const ParserService_1 = require("../services/ParserService");
function default_1(app) {
    let pS = new ParserService_1.ParserService();
    let ncS = new note_category_service_1.NoteCategoryService();
    /* GET */
    // FINISHED
    app.get('/loggedIn/getCat_Notes', function (req, res) {
        let answers = [];
        ncS.getAll()
            .then((noteCategories) => {
            for (let category of noteCategories) {
                answers.push({
                    id: category.key,
                    name: category.name
                });
            }
            return res.json(answers);
        });
    });
    /* POST */
    // FINISHED
    app.post('/loggedIn/newNoteCategory', function (req, res) {
        let data = pS.parse(req.body, NewNoteCategoryRequest_1.NewNoteCategoryRequestStructure);
        if (!data)
            return res.sendStatus(400);
        ncS.insert(new note_category_model_1.NoteCategory(null, data.name)).then(noteCategory => {
            res.sendStatus(200);
        });
    });
    // FINISHED
    app.post('/loggedIn/deleteNoteCategory', function (req, res) {
        let data = pS.parse(req.body, DeleteNoteCategoryRequest_1.DeleteNoteCategoryRequestStructure);
        ncS.delete(data.id);
        res.sendStatus(200);
    });
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
//# sourceMappingURL=noteCategory.js.map