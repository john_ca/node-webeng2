"use strict";
const teacher_model_1 = require('../models/teacher.model');
const GetTeachersRequest_1 = require('../API/teacher/GetTeachersRequest');
const GetOwnEmailRequest_1 = require('../API/teacher/GetOwnEmailRequest');
const GetOwnRightsRequest_1 = require('../API/teacher/GetOwnRightsRequest');
const teacher_service_1 = require("../services/teacher.service");
const ParserService_1 = require("../services/ParserService");
const NewTeacherRequest_1 = require("../API/teacher/NewTeacherRequest");
const DeleteTeacherRequest_1 = require("../API/teacher/DeleteTeacherRequest");
const UpdatePasswordRequest_1 = require("../API/teacher/UpdatePasswordRequest");
const UpdateEmailRequest_1 = require("../API/teacher/UpdateEmailRequest");
const BCryptService_1 = require("../services/BCryptService");
var passport = require('passport');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
function default_1(app) {
    let pS = new ParserService_1.ParserService();
    let bS = new BCryptService_1.default();
    let tS = new teacher_service_1.TeacherService();
    /* GET */
    app.get('/loggedIn/getDisplayableTeacher', function (req, res) {
        let schoolKey = req.session['school'];
        tS.getAll(schoolKey).then((teachers) => {
            let answer = [];
            for (let teacher of teachers)
                answer.push({
                    id: teacher.key,
                    prename: teacher.prename,
                    surname: teacher.surname,
                    username: teacher.username,
                    email: teacher.email,
                    admin: teacher.admin,
                    rolename: teacher.admin ? "Admin" : "User"
                });
            res.json(answer);
        });
    });
    // FINISHED
    app.get('/loggedIn/getTeachers', function (req, res) {
        let schoolKey = req.session['school'];
        tS.getAll(schoolKey).then(teachers => {
            let teacherResponseObjects = [];
            for (let teacher of teachers) {
                teacherResponseObjects.push(new GetTeachersRequest_1.GetTeachersResponseObject(teacher.key, teacher.prename, teacher.surname, teacher.username, teacher.email, teacher.admin));
            }
            res.json(teacherResponseObjects);
        });
    });
    // FINISHED
    app.get('/loggedIn/getOwnRights', function (req, res) {
        tS.get(req.session['user']).then(teacher => {
            res.json(new GetOwnRightsRequest_1.GetOwnRightsResponseObject(teacher.admin));
        });
    });
    // FINISHED
    app.get('/loggedIn/getOwnEmail', function (req, res) {
        tS.get(req.session['user']).then(teacher => {
            res.json(new GetOwnEmailRequest_1.GetOwnEmailResponseObject(teacher.email));
        });
    });
    /* POST */
    // FINISHED
    app.post('/loggedIn/newTeacher', function (req, res) {
        let data = pS.parse(req.body, NewTeacherRequest_1.NewTeacherRequestStructure);
        if (!data)
            return res.sendStatus(400);
        return bS.hash(data.password).then(hash => {
            tS.insert(new teacher_model_1.Teacher(null, data.prename, data.surname, data.username, data.email, false, hash, req.session["school"]))
                .then(() => {
                res.sendStatus(200);
            });
        });
    });
    // FINISHED
    app.post('/loggedIn/deleteTeacher', function (req, res) {
        let data = pS.parse(req.body, DeleteTeacherRequest_1.DeleteTeacherRequestStructure);
        if (!data)
            return res.sendStatus(400);
        tS.delete(data.id)
            .then(() => {
            res.sendStatus(200);
        });
    });
    app.post('/loggedIn/updatePassword', function (req, res) {
        let data = pS.parse(req.body, UpdatePasswordRequest_1.UpdatePasswordRequestStructure);
        if (!data)
            return res.sendStatus(400);
        // Get key of logged in teacher
        let key = req.session['user'];
        tS.get(key)
            .then((teacher) => {
            // Hash Password
            return bS.hash(data.newPassword).then(hash => {
                teacher.password = hash;
                return tS.update(teacher)
                    .then(() => {
                    res.json({ correct: true });
                });
            });
        });
    });
    // FINISHED
    app.post('/loggedIn/updateEmail', function (req, res) {
        let data = pS.parse(req.body, UpdateEmailRequest_1.UpdateEmailRequestStructure);
        if (!data)
            return res.sendStatus(400);
        tS.get(req.session['user']).then(teacher => {
            teacher.email = data.email;
            tS.update(teacher)
                .then(() => {
                res.sendStatus(200);
            });
        });
    });
    app.get('/auth/google', passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/plus.login'] }));
    app.get('/auth/google/callback', passport.authenticate('google', {
        successRedirect: '/',
        failureRedirect: '/'
    }), function (req, res) {
        res.send("hi");
    });
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
//# sourceMappingURL=teacher.js.map