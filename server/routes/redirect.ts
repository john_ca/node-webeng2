import {Response} from "express";

export default function (app) {
    app.get('/', function (req, res: Response, next) {
        if (req.session['user'] && req.session['school']) {
            return res.redirect('/loggedIn');
        } else {
            next();
        }
    })
}