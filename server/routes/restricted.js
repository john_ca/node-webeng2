"use strict";
function default_1(app) {
    app.all('/loggedIn*', function (req, res, next) {
        if (req.session["user"] && req.session['school'])
            next();
        else {
            res.sendStatus(401); //Not logged in
        }
    });
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
//# sourceMappingURL=restricted.js.map