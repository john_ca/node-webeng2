import {ParserService} from '../services/ParserService';
import {Response, Request} from "express";
import BCryptService from "../services/BCryptService";
import {LoginRequest, LoginRequestStructure} from "../API/login/LoginRequest";
import gcloud = require('google-cloud');

let datastore = gcloud.datastore({
    projectId: 'node-klassenbuch',
    keyFilename: './Node-Klassenbuch-1ffc73dd3e10.json'
});


export default function (app) {
    let pS = new ParserService();
    let bS = new BCryptService();
    app.post('/login', function (req: Request, res: Response, next) {
        let data: LoginRequest = pS.parse(req.body, LoginRequestStructure);
        if (!data) res.status(400);

        datastore.createQuery('Teacher')
            .filter('username', data.username)
            .run()
            .then((results) => {
                for (let result of results[0]) {
                    if (result.schoolKey == data.f_school) {
                        let user = results[0][0];
                        return bS.compare(data.password, user.password).then(result => {
                            if (result) {
                                req.session["user"] = user[datastore.KEY].id;
                                req.session["school"] = user.schoolKey;
                                return res.status(200).json({error: false});
                            } else {
                                return res.status(200).json({error: true});
                            }
                        });
                    }
                }
                res.status(200).json({error: true});
            });
    });
    app.post('/logout', function (req, res) {
        req.session = null;
        res.json({error: false});
    });
};
