import {Teacher} from '../models/teacher.model';
import {GetTeachersResponseObject, GetTeachersResponse} from '../API/teacher/GetTeachersRequest';
import {GetOwnEmailResponseObject} from '../API/teacher/GetOwnEmailRequest';
import {GetOwnRightsResponseObject} from '../API/teacher/GetOwnRightsRequest';
import {Response, Request} from "express";
import {TeacherService} from "../services/teacher.service";
import {ParserService} from "../services/ParserService";
import {NewTeacherRequest, NewTeacherRequestStructure} from "../API/teacher/NewTeacherRequest";
import {DeleteTeacherRequest, DeleteTeacherRequestStructure} from "../API/teacher/DeleteTeacherRequest";
import {UpdatePasswordRequest, UpdatePasswordRequestStructure} from "../API/teacher/UpdatePasswordRequest";
import {UpdateEmailRequest, UpdateEmailRequestStructure} from "../API/teacher/UpdateEmailRequest";
import BCryptService from "../services/BCryptService";
import {GetDisplayableTeacherResponse} from "../API/teacher/GetDisplayableTeacherRequest";
import {UpdateTeacherRequest, UpdateTeacherRequestStructure} from "../API/teacher/UpdateTeacherRequest";

var passport = require('passport');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

export default function (app) {
    let pS = new ParserService();
    let bS = new BCryptService();
    let tS = new TeacherService();

    /* GET */

    app.get('/loggedIn/getDisplayableTeacher', function (req, res) {
        let schoolKey = req.session['school'];
        tS.getAll(schoolKey).then((teachers: Teacher[]) => {
            let answer: GetDisplayableTeacherResponse = [];
            for (let teacher of teachers)
                answer.push({
                    id: teacher.key,
                    prename: teacher.prename,
                    surname: teacher.surname,
                    username: teacher.username,
                    email: teacher.email,
                    admin: teacher.admin,
                    rolename: teacher.admin ? "Admin" : "User"
                });
            res.json(answer);
        });
    });

    /* POST */
    app.post('/loggedIn/updateTeacher', function (req: Request|any, res: Response) {
        let data: UpdateTeacherRequest = pS.parse(req.body, UpdateTeacherRequestStructure);
        if (!data) return res.sendStatus(400);
        tS.get(data.id)
            .then((teacher: Teacher) => {
                return tS.update({
                    key: data.id,
                    prename: data.prename,
                    surname: data.surname,
                    username: teacher.username,
                    password: teacher.password,
                    email: data.email,
                    admin: teacher.admin,
                    schoolKey: req.session["school"],
                }).then(() => {
                    res.sendStatus(200);
                });
            });
    });

    // FINISHED
    app.get('/loggedIn/getTeachers', function (req, res) {
        let schoolKey = req.session['school'];
        tS.getAll(schoolKey).then(teachers => {
            let teacherResponseObjects: GetTeachersResponse = [];
            for (let teacher of teachers) {
                teacherResponseObjects.push(new GetTeachersResponseObject(teacher.key, teacher.prename, teacher.surname, teacher.username, teacher.email, teacher.admin));
            }
            res.json(teacherResponseObjects);
        });
    });

    // FINISHED
    app.get('/loggedIn/getOwnRights', function (req, res) {
        tS.get(req.session['user']).then(teacher => {
            res.json(new GetOwnRightsResponseObject(teacher.admin));
        });
    });

    // FINISHED
    app.get('/loggedIn/getOwnEmail', function (req, res) {
        tS.get(req.session['user']).then(teacher => {
            res.json(new GetOwnEmailResponseObject(teacher.email));
        });
    });

    /* POST */

    // FINISHED
    app.post('/loggedIn/newTeacher', function (req: Request|any, res: Response) {
        let data: NewTeacherRequest = pS.parse(req.body, NewTeacherRequestStructure);
        if (!data) return res.sendStatus(400);
        return bS.hash(data.password).then(hash => {
            tS.insert(new Teacher(null, data.prename, data.surname, data.username, data.email, false, hash, req.session["school"]))
                .then(() => {
                    res.sendStatus(200);
                });
        });
    });

    // FINISHED
    app.post('/loggedIn/deleteTeacher', function (req: Request, res: Response) {
        let data: DeleteTeacherRequest = pS.parse(req.body, DeleteTeacherRequestStructure);
        if (!data) return res.sendStatus(400);
        tS.delete(data.id)
            .then(() => {
                res.sendStatus(200);
            });
    });

    app.post('/loggedIn/updatePassword', function (req: Request, res: Response) {
        let data: UpdatePasswordRequest = pS.parse(req.body, UpdatePasswordRequestStructure);
        if (!data) return res.sendStatus(400);
        // Get key of logged in teacher
        let key = req.session['user'];
        tS.get(key)
            .then((teacher: Teacher) => {
                // Hash Password
                return bS.hash(data.newPassword).then(hash => {
                    teacher.password = hash;
                    return tS.update(teacher)
                        .then(() => {
                            res.json({correct: true});
                        });
                });
            });
    });

    // FINISHED
    app.post('/loggedIn/updateEmail', function (req: Request, res: Response) {
        let data: UpdateEmailRequest = pS.parse(req.body, UpdateEmailRequestStructure);
        if (!data) return res.sendStatus(400);
        tS.get(req.session['user']).then(teacher => {
            teacher.email = data.email;
            tS.update(teacher)
                .then(() => {
                    res.sendStatus(200);
                });
        });
    });

    app.get('/auth/google', passport.authenticate('google', {scope: ['https://www.googleapis.com/auth/plus.login']}));
    app.get('/auth/google/callback', passport.authenticate('google', {
        successRedirect: '/',
        failureRedirect: '/'
    }), function (req, res) {
        res.send("hi");
    });
}