"use strict";
const student_service_1 = require('../services/student.service');
const teacher_service_1 = require('../services/teacher.service');
const ParserService_1 = require("../services/ParserService");
const UpdateNoteRequest_1 = require("../API/note/UpdateNoteRequest");
const NewNoteRequest_1 = require("../API/note/NewNoteRequest");
const DeleteNoteRequest_1 = require("../API/note/DeleteNoteRequest");
const ExcuseNoteRequest_1 = require("../API/note/ExcuseNoteRequest");
const note_service_1 = require("../services/note.service");
const note_category_service_1 = require("../services/note-category.service");
const school_class_service_1 = require("../services/school-class.service");
function default_1(app) {
    let pS = new ParserService_1.ParserService();
    let nS = new note_service_1.NoteService();
    let ncS = new note_category_service_1.NoteCategoryService();
    let tS = new teacher_service_1.TeacherService();
    let sS = new student_service_1.StudentService();
    let cS = new school_class_service_1.SchoolClassService();
    app.post('/loggedIn/updateNote', function (req, res) {
        let data = pS.parse(req.body, UpdateNoteRequest_1.UpdateNoteRequestStructure);
        if (!data)
            return res.sendStatus(400);
        nS.update({
            key: data.id,
            teacherKey: data.f_teacher,
            studentKey: data.f_student,
            noteCategoryKey: data.f_category,
            time: new Date().getTime(),
            text: data.text,
            excused: data.excused,
            schoolKey: req.session["school"]
        }).then(() => {
            res.sendStatus(200);
        });
    });
    app.post('/loggedIn/newNote', function (req, res) {
        let data = pS.parse(req.body, NewNoteRequest_1.NewNoteRequestStructure);
        if (!data)
            return res.sendStatus(400);
        nS.insert({
            key: null,
            teacherKey: req.session["user"],
            studentKey: data.f_student,
            noteCategoryKey: data.f_category,
            time: new Date().getTime(),
            text: data.text,
            excused: data.excused,
            schoolKey: req.session["school"]
        }).then(() => {
            res.sendStatus(200);
        });
    });
    app.post('/loggedIn/deleteNote', function (req, res) {
        let data = pS.parse(req.body, DeleteNoteRequest_1.DeleteNoteRequestStructure);
        if (!data)
            return res.sendStatus(400);
        nS.delete(data.id).then(() => {
            res.sendStatus(200);
        });
    });
    app.post('/loggedIn/excuseNote', function (req, res) {
        let data = pS.parse(req.body, ExcuseNoteRequest_1.ExcuseNoteRequestStructure);
        if (!data)
            return res.sendStatus(400);
        nS.get(data.id)
            .then((note) => {
            note.excused = !note.excused;
            return nS.update(note)
                .then(() => {
                res.sendStatus(200);
            });
        });
    });
    app.get('/loggedIn/getLastNotes', function (req, res) {
        let answers = [];
        let teacherKey = req.session["user"];
        let schoolKey = req.session["school"];
        return sS.getAll(schoolKey).then((studentEntries) => {
            return ncS.getAll().then((categoryEntries) => {
                let students = {};
                let categories = {};
                for (let category of categoryEntries)
                    categories[category.key] = category;
                for (let student of studentEntries)
                    students[student.key] = student;
                return nS.getAll(schoolKey)
                    .then((notes) => {
                    notes = notes.filter(val => (val.teacherKey == teacherKey));
                    notes = notes.slice(0, 5);
                    answers = notes.map(note => {
                        return {
                            id: note.key,
                            text: note.text,
                            excused: note.excused,
                            student: (students[note.studentKey]) ? students[note.studentKey].prename + " " + students[note.studentKey].surname : "Kein Schüler",
                            category: (categories[note.noteCategoryKey]) ? categories[note.noteCategoryKey].name : "Keine Kategorie"
                        };
                    });
                    return res.json(answers);
                });
            });
        });
    });
    app.get('/loggedIn/getLastNotesOfClass', function (req, res) {
        let answers = {};
        let teacherKey = req.session["user"];
        let schoolKey = req.session["school"];
        return sS.getAll(schoolKey).then((studentEntries) => {
            return ncS.getAll().then((categoryEntries) => {
                return cS.getAll(schoolKey).then((classEntries) => {
                    let students = {};
                    let classes = {};
                    let categories = {};
                    for (let schoolClass of classEntries)
                        classes[schoolClass.key] = schoolClass;
                    for (let category of categoryEntries)
                        categories[category.key] = category;
                    for (let student of studentEntries)
                        students[student.key] = student;
                    return nS.getAll(schoolKey)
                        .then((notes) => {
                        for (let note of notes) {
                            try {
                                let sClass = classes[students[note.studentKey].classKey];
                                if (sClass.teacherKey == teacherKey) {
                                    let className = sClass.level + '' + sClass.identifier; //Compose Classname
                                    let elem = {
                                        id: note.key,
                                        text: note.text,
                                        excused: note.excused,
                                        student: (students[note.studentKey]) ? students[note.studentKey].prename + " " + students[note.studentKey].surname : "Kein Schüler",
                                        category: (categories[note.noteCategoryKey]) ? categories[note.noteCategoryKey].name : "Keine Kategorie"
                                    };
                                    answers[className] ? answers[className].push(elem) : answers[className] = [elem]; //Create array in array, if not already existing
                                }
                            }
                            catch (e) {
                            }
                        }
                        res.json(answers);
                    });
                });
            });
        });
    });
    app.get('/loggedIn/getDisplayableNotes', function (req, res) {
        let answers = [];
        let schoolKey = req.session["school"];
        tS.getAll(schoolKey).then((teacherEntries) => {
            return sS.getAll(schoolKey).then((studentEntries) => {
                return ncS.getAll().then((categoryEntries) => {
                    let students = [];
                    let teachers = [];
                    let categories = [];
                    for (let category of categoryEntries)
                        categories[category.key] = category;
                    for (let student of studentEntries)
                        students[student.key] = student;
                    for (let teacher of teacherEntries)
                        teachers[teacher.key] = teacher;
                    return nS.getAll(schoolKey)
                        .then((notes) => {
                        for (let note of notes)
                            answers.push({
                                id: note.key,
                                f_teacher: note.teacherKey,
                                f_student: note.studentKey,
                                f_category: note.noteCategoryKey,
                                text: note.text,
                                excused: note.excused,
                                teacher: (teachers[note.teacherKey]) ? teachers[note.teacherKey].prename + " " + teachers[note.teacherKey].surname : "Kein Lehrer",
                                student: (students[note.studentKey]) ? students[note.studentKey].prename + " " + students[note.studentKey].surname : "Kein Schüler",
                                category: (categories[note.noteCategoryKey]) ? categories[note.noteCategoryKey].name : "Keine Kategorie"
                            });
                        return res.json(answers);
                    });
                });
            });
        });
    });
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
//# sourceMappingURL=note.js.map