import { GetNoteCategoriesResponse, GetNoteCategoriesResponseObject } from './../API/noteCategory/GetNoteCategoriesRequest';
import { NoteCategoryService } from './../services/note-category.service';
import { NoteCategory } from './../models/note-category.model';
import {Response, Request} from "express";
import {NewNoteCategoryRequest, NewNoteCategoryRequestStructure} from "../API/noteCategory/NewNoteCategoryRequest";
import { DeleteNoteCategoryRequest, DeleteNoteCategoryRequestStructure } from "../API/noteCategory/DeleteNoteCategoryRequest";

import {ParserService} from "../services/ParserService";

export default function (app) {
    let pS = new ParserService();
    let ncS = new NoteCategoryService();

    /* GET */

    // FINISHED
    app.get('/loggedIn/getCat_Notes', function (req, res) {
        let answers: GetNoteCategoriesResponse = [];
        ncS.getAll()
            .then((noteCategories: NoteCategory[])=>{
                for (let category of noteCategories) {
                    answers.push({
                        id: category.key,
                        name: category.name
                    });
                }
                return res.json(answers);
            })
    });

    /* POST */

    // FINISHED
    app.post('/loggedIn/newNoteCategory', function (req: Request, res: Response) {
        let data: NewNoteCategoryRequest = pS.parse(req.body, NewNoteCategoryRequestStructure);
        if(!data) return res.sendStatus(400);

        ncS.insert(new NoteCategory(null, data.name)).then(noteCategory => {
            res.sendStatus(200);
        });
    });

    // FINISHED
    app.post('/loggedIn/deleteNoteCategory', function (req: Request, res: Response) {
        let data: DeleteNoteCategoryRequest = pS.parse(req.body, DeleteNoteCategoryRequestStructure);

        ncS.delete(data.id);

        res.sendStatus(200);
    });
}
