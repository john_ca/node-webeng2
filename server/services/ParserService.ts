export class ParserService {
    parse(data: Object, specs: Object): any {
        let parsed = {};
        if (typeof data != 'object') return null;
        for (let spec in specs) {
            let type = specs[spec];
            if (typeof data[spec] == "undefined") return null;
            switch (type) {
                case 'number':
                    parsed[spec] = parseInt(data[spec]);
                    if (isNaN(data[spec])) return null;
                    break;
                case 'string':
                    parsed[spec] = data[spec] + "";
                    if (parsed[spec].trim() == '') return null;
                    break;
                case 'boolean':
                    parsed[spec] = (data[spec] === 'true');
                    break;
                case 'object':
                    parsed[spec] = data[spec];
                    break;
            }
        }
        return parsed;
    }
}