import {hash} from "bcryptjs";
import {compare} from "bcryptjs";
import {genSalt} from "bcryptjs";

export default class BCryptService {
    private saltRounds = 10;

    public genSalt(): Promise<string> {
        return new Promise<string>(
            (resolve, reject)=> {
                genSalt((err: Error, salt: string) => {
                    if (err) return reject();
                    return resolve(salt.substr(7));
                })
            }
        )
    }

    public hash(raw: string): Promise<string> {
        return new Promise<string | Error>(
            (resolve, reject)=> {
                hash(raw, this.saltRounds, (err: Error, encrypted: string)=> {
                    if (err) return reject(err);
                    return resolve(encrypted);
                });
            }
        );
    }

    public compare(raw: string, hash: string): Promise<boolean> {
        return new Promise<boolean | Error>(
            (resolve, reject)=> {
                compare(raw, hash, function (err: Error, res: boolean) {
                    if (err) return reject(err);
                    return resolve(res);
                });
            }
        )
    }
}