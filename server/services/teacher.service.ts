import {Teacher} from '../models/teacher.model';
import gcloud = require('google-cloud');

let datastore = gcloud.datastore({
    projectId: 'node-klassenbuch',
    keyFilename: './Node-Klassenbuch-1ffc73dd3e10.json'
});

export class TeacherService {
    insert(teacher: Teacher): Promise<Teacher> {
        let key = datastore.key('Teacher');
        let entity = {
            key: key,
            data: {
                prename: teacher.prename,
                surname: teacher.surname,
                username: teacher.username,
                password: teacher.password,
                email: teacher.email,
                admin: teacher.admin,
                schoolKey: teacher.schoolKey
            }
        };

        return datastore.insert(entity).then((data) => {
            teacher.key = key.id;
            return teacher;
        });
    }

    update(teacher: Teacher): Promise<Teacher> {
        let entity = {
            key: datastore.key(['Teacher', +teacher.key]),
            data: {
                prename: teacher.prename,
                surname: teacher.surname,
                username: teacher.username,
                password: teacher.password,
                email: teacher.email,
                admin: teacher.admin,
                schoolKey: teacher.schoolKey,
                googleId: teacher.googleId
            }
        };
        return datastore.update(entity);
    }

    delete(key: any): Promise<any> {
        return datastore.delete(datastore.key(['Teacher', +key]));
    }

    getAll(schoolKey): Promise<Teacher[]> {
        return datastore.createQuery('Teacher').run().then((results) => {
            let result: Teacher[] = results[0].map((val) => {
                val.key = val[datastore.KEY].id;
                return val;
            });
            return result.filter(val => (val.schoolKey == schoolKey));
        });
    }

    get(key: any): Promise<Teacher> {
        return datastore.get(datastore.key(['Teacher', +key])).then((results) => {
            results[0].key = results[0][datastore.KEY].id;
            return results[0];
        });
    }

    getAllGoogle(): Promise<Teacher[]> {
        return datastore.createQuery('Teacher').run().then((results) => {
            let result: Teacher[] = results[0];
            return result.map((val) => {
                val.key = val[datastore.KEY].id;
                return val;
            });
        });
    }
}