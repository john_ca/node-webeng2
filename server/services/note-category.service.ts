import {NoteCategory} from '../models/note-category.model';
import gcloud = require('google-cloud');

var datastore = gcloud.datastore({
    projectId: 'node-klassenbuch',
    keyFilename: './Node-Klassenbuch-1ffc73dd3e10.json'
});

export class NoteCategoryService {
    insert(noteCategory: NoteCategory): Promise<NoteCategory> {
        let key = datastore.key('NoteCategory');
        let entity = {
            key: key,
            data: {
                name: noteCategory.name
            }
        };

        return datastore.insert(entity).then((data) => {
            noteCategory.key = key.id;
            return noteCategory;
        });
    }

    update(noteCategory: NoteCategory): Promise<NoteCategory> {
        let entity = {
            key: datastore.key(['NoteCategory', +noteCategory.key]),
            data: {
                name: noteCategory.name
            }
        };

        return datastore.update(entity).then((data) => {
            return noteCategory;
        });
    }

    delete(key: string): Promise<any> {
        return datastore.delete(datastore.key(['NoteCategory', +key]));
    }

    getAll(): Promise<NoteCategory[]> {
        return datastore.createQuery('NoteCategory').run().then((results) => {
            return results[0].map((val) => {
                val.key = val[datastore.KEY].id;
                return val;
            });
        });
    }

    get(key: string): Promise<NoteCategory> {
        return datastore.get(datastore.key(['NoteCategory', +key])).then((results) => {
            results[0].key = key;
            return results[0];
        });
    }
}