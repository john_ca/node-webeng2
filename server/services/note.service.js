"use strict";
const gcloud = require('google-cloud');
let datastore = gcloud.datastore({
    projectId: 'node-klassenbuch',
    keyFilename: './Node-Klassenbuch-1ffc73dd3e10.json'
});
class NoteService {
    insert(note) {
        let key = datastore.key('Note');
        let entity = {
            key: key,
            data: {
                teacherKey: note.teacherKey,
                studentKey: note.studentKey,
                noteCategoryKey: note.noteCategoryKey,
                schoolKey: note.schoolKey,
                time: note.time,
                text: note.text,
                excused: note.excused
            }
        };
        return datastore.insert(entity).then(data => {
            note.key = key.id;
            return note;
        });
    }
    update(note) {
        let entity = {
            key: datastore.key(['Note', +note.key]),
            data: {
                teacherKey: note.teacherKey,
                studentKey: note.studentKey,
                schoolKey: note.schoolKey,
                noteCategoryKey: note.noteCategoryKey,
                time: note.time,
                text: note.text,
                excused: note.excused
            }
        };
        return datastore.update(entity);
    }
    delete(key) {
        return datastore.delete(datastore.key(['Note', +key]));
    }
    getAll(schoolKey) {
        return datastore.createQuery('Note').order('time', { descending: true }).run().then((results) => {
            let notes = results[0].map((val) => {
                val.key = val[datastore.KEY].id;
                return val;
            });
            return notes.filter(val => (val.schoolKey == schoolKey));
        });
    }
    get(key) {
        return datastore.get(datastore.key(['Note', +key])).then((results) => {
            results[0].key = key;
            return results[0];
        });
    }
}
exports.NoteService = NoteService;
//# sourceMappingURL=note.service.js.map