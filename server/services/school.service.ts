import gcloud = require('google-cloud');

let datastore = gcloud.datastore({
    projectId: 'node-klassenbuch',
    keyFilename: './Node-Klassenbuch-1ffc73dd3e10.json'
});

export class SchoolService {
    insert(school: School): Promise<School> {
        let data = {
            name: school.name,
            address: school.address
        };
        let key = datastore.key('School');
        let entity = {
            key: key,
            data: data
        };
        return datastore.insert(entity).then(() => {
            school.key = key.id;
            return school;
        });
    }

    update(school: School): Promise<School> {

        var entity = {
            key: datastore.key(['Teacher', +school.key]),
            data: {
                name: school.name,
                address: school.address
            }
        };

        return datastore.update(entity).then((data) => {
            return school;
        });
    }

    getAll(): Promise<School[]> {
        return datastore.createQuery('School').run().then((results) => {
            return results[0].map((val) => {
                val.key = val[datastore.KEY].id;
                return val;
            });
        });
    }

    get(key: string): Promise<School> {
        return datastore.get(datastore.key(['School', +key])).then((results) => {
            results[0].key = key;
            return results[0];
        });
    }
}