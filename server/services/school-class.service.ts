import {TeacherService} from './teacher.service';
import gcloud = require('google-cloud');
import {SchoolClass} from "../models/school-class.model";

var datastore = gcloud.datastore({
    projectId: 'node-klassenbuch',
    keyFilename: './Node-Klassenbuch-1ffc73dd3e10.json'
});

export class SchoolClassService {

    insert(schoolClass: SchoolClass): Promise<null> {
        let key = datastore.key('Class');
        let entity = {
            key: key,
            data: {
                level: schoolClass.level,
                identifier: schoolClass.identifier,
                teacherKey: schoolClass.teacherKey,
                schoolKey: schoolClass.schoolKey
            }
        };
        return datastore.insert(entity).then(data => {
            schoolClass.key = key.id;
            return schoolClass;
        });
    }

    update(schoolClass: SchoolClass) {
        let entity = {
            key: datastore.key(['Class', +schoolClass.key]),
            data: {
                level: schoolClass.level,
                identifier: schoolClass.identifier,
                teacherKey: schoolClass.teacherKey,
                schoolKey: schoolClass.schoolKey
            }
        };
        return datastore.update(entity).then(data => {
            return schoolClass;
        });
    }

    delete(key: string): Promise<null> {
        return datastore.delete(datastore.key(['Class', +key]));
    }

    getAll(schoolKey): Promise<SchoolClass[]> {
        return datastore.createQuery('Class').run().then((results) => {
            let result: SchoolClass[] = results[0].map((val) => {
                val.key = val[datastore.KEY].id;
                return val;
            });
            return result.filter(val => (val.schoolKey == schoolKey));
        });
    }

    get(key: string): Promise<SchoolClass> {
        return datastore.get(datastore.key(['Class', +key])).then((results) => {
            results[0].key = key;
            return results[0];
        });
    }
}