import gcloud = require('google-cloud');
import {Note} from "../models/note.model";

let datastore = gcloud.datastore({
    projectId: 'node-klassenbuch',
    keyFilename: './Node-Klassenbuch-1ffc73dd3e10.json'
});

export class NoteService {
    insert(note: Note): Promise<Note> {
        let key = datastore.key('Note');
        let entity = {
            key: key,
            data: {
                teacherKey: note.teacherKey,
                studentKey: note.studentKey,
                noteCategoryKey: note.noteCategoryKey,
                schoolKey: note.schoolKey,
                time: note.time,
                text: note.text,
                excused: note.excused
            }
        };
        return datastore.insert(entity).then(data => {
            note.key = key.id;
            return note;
        });
    }

    update(note: Note): Promise<Note> {
        let entity = {
            key: datastore.key(['Note', +note.key]),
            data: {
                teacherKey: note.teacherKey,
                studentKey: note.studentKey,
                schoolKey: note.schoolKey,
                noteCategoryKey: note.noteCategoryKey,
                time: note.time,
                text: note.text,
                excused: note.excused
            }
        };
        return datastore.update(entity);
    }

    delete(key: string): Promise<any> {
        return datastore.delete(datastore.key(['Note', +key]));
    }

    getAll(schoolKey): Promise<Note[]> {
        return datastore.createQuery('Note').order('time', {descending: true}).run().then((results) => {
            let notes: Note[] = results[0].map((val) => {
                val.key = val[datastore.KEY].id;
                return val;
            });
            return notes.filter(val => (val.schoolKey == schoolKey));
        });
    }

    get(key: string): Promise<Note> {
        return datastore.get(datastore.key(['Note', +key])).then((results) => {
            results[0].key = key;
            return results[0];
        });
    }
}