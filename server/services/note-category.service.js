"use strict";
const gcloud = require('google-cloud');
var datastore = gcloud.datastore({
    projectId: 'node-klassenbuch',
    keyFilename: './Node-Klassenbuch-1ffc73dd3e10.json'
});
class NoteCategoryService {
    insert(noteCategory) {
        let key = datastore.key('NoteCategory');
        let entity = {
            key: key,
            data: {
                name: noteCategory.name
            }
        };
        return datastore.insert(entity).then((data) => {
            noteCategory.key = key.id;
            return noteCategory;
        });
    }
    update(noteCategory) {
        let entity = {
            key: datastore.key(['NoteCategory', +noteCategory.key]),
            data: {
                name: noteCategory.name
            }
        };
        return datastore.update(entity).then((data) => {
            return noteCategory;
        });
    }
    delete(key) {
        return datastore.delete(datastore.key(['NoteCategory', +key]));
    }
    getAll() {
        return datastore.createQuery('NoteCategory').run().then((results) => {
            return results[0].map((val) => {
                val.key = val[datastore.KEY].id;
                return val;
            });
        });
    }
    get(key) {
        return datastore.get(datastore.key(['NoteCategory', +key])).then((results) => {
            results[0].key = key;
            return results[0];
        });
    }
}
exports.NoteCategoryService = NoteCategoryService;
//# sourceMappingURL=note-category.service.js.map