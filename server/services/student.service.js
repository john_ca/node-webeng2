"use strict";
const gcloud = require('google-cloud');
var datastore = gcloud.datastore({
    projectId: 'node-klassenbuch',
    keyFilename: './Node-Klassenbuch-1ffc73dd3e10.json'
});
class StudentService {
    insert(student) {
        let entity = {
            key: datastore.key('Student'),
            data: {
                prename: student.prename,
                surname: student.surname,
                classKey: student.classKey,
                schoolKey: student.schoolKey
            }
        };
        return datastore.insert(entity);
    }
    update(student) {
        let entity = {
            key: datastore.key(['Student', +student.key]),
            data: {
                prename: student.prename,
                surname: student.surname,
                classKey: student.classKey,
                schoolKey: student.schoolKey
            }
        };
        return datastore.update(entity);
    }
    delete(key) {
        return datastore.delete(datastore.key(['Student', +key]));
    }
    getAll(schoolKey) {
        return datastore.createQuery('Student').run().then((results) => {
            let result = results[0].map((val) => {
                val.key = val[datastore.KEY].id;
                return val;
            });
            return result.filter(val => (val.schoolKey == schoolKey));
        });
    }
    get(key) {
        return datastore.get(datastore.key(+key)).then(results => {
            results[0].key = results[0][datastore.KEY].id;
            return results[0];
        });
    }
}
exports.StudentService = StudentService;
//# sourceMappingURL=student.service.js.map