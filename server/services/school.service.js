"use strict";
const gcloud = require('google-cloud');
let datastore = gcloud.datastore({
    projectId: 'node-klassenbuch',
    keyFilename: './Node-Klassenbuch-1ffc73dd3e10.json'
});
class SchoolService {
    insert(school) {
        let data = {
            name: school.name,
            address: school.address
        };
        let key = datastore.key('School');
        let entity = {
            key: key,
            data: data
        };
        return datastore.insert(entity).then(() => {
            school.key = key.id;
            return school;
        });
    }
    update(school) {
        var entity = {
            key: datastore.key(['Teacher', +school.key]),
            data: {
                name: school.name,
                address: school.address
            }
        };
        return datastore.update(entity).then((data) => {
            return school;
        });
    }
    getAll() {
        return datastore.createQuery('School').run().then((results) => {
            return results[0].map((val) => {
                val.key = val[datastore.KEY].id;
                return val;
            });
        });
    }
    get(key) {
        return datastore.get(datastore.key(['School', +key])).then((results) => {
            results[0].key = key;
            return results[0];
        });
    }
}
exports.SchoolService = SchoolService;
//# sourceMappingURL=school.service.js.map