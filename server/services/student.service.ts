import gcloud = require('google-cloud');
import {Student} from "../models/student.model";

var datastore = gcloud.datastore({
    projectId: 'node-klassenbuch',
    keyFilename: './Node-Klassenbuch-1ffc73dd3e10.json'
});

export class StudentService {
    insert(student: Student): Promise<Student> {
        let entity = {
            key: datastore.key('Student'),
            data: {
                prename: student.prename,
                surname: student.surname,
                classKey: student.classKey,
                schoolKey: student.schoolKey
            }
        };
        return datastore.insert(entity);
    }

    update(student: Student): Promise<any> {
        let entity = {
            key: datastore.key(['Student', +student.key]),
            data: {
                prename: student.prename,
                surname: student.surname,
                classKey: student.classKey,
                schoolKey: student.schoolKey
            }
        };
        return datastore.update(entity);
    }

    delete(key: string): Promise<null> {
        return datastore.delete(datastore.key(['Student', +key]));
    }

    getAll(schoolKey): Promise<Student[]> {
        return datastore.createQuery('Student').run().then((results) => {
            let result: Student[] = results[0].map((val) => {
                val.key = val[datastore.KEY].id;
                return val;
            });
            return result.filter(val => (val.schoolKey == schoolKey));
        });
    }

    get(key: string): Promise<Student> {
        return datastore.get(datastore.key(+key)).then(results => {
            results[0].key = results[0][datastore.KEY].id;
            return results[0];
        });
    }
}