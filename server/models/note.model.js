"use strict";
class Note {
    constructor(key, teacherKey, studentKey, noteCategoryKey, time, text, excused, schoolKey) {
        this.key = key;
        this.teacherKey = teacherKey;
        this.studentKey = studentKey;
        this.noteCategoryKey = noteCategoryKey;
        this.time = time;
        this.text = text;
        this.excused = excused;
        this.schoolKey = schoolKey;
    }
}
exports.Note = Note;
//# sourceMappingURL=note.model.js.map