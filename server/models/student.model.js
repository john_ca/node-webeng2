"use strict";
class Student {
    constructor(key, prename, surname, classKey, schoolKey) {
        this.key = key;
        this.prename = prename;
        this.surname = surname;
        this.classKey = classKey;
        this.schoolKey = schoolKey;
    }
}
exports.Student = Student;
//# sourceMappingURL=student.model.js.map