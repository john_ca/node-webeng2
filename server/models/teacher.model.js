"use strict";
class Teacher {
    constructor(key, prename, surname, username, email, admin, password, schoolKey, googleId) {
        this.key = key;
        this.prename = prename;
        this.surname = surname;
        this.username = username;
        this.email = email;
        this.admin = admin;
        this.password = password;
        this.schoolKey = schoolKey;
        this.googleId = googleId;
    }
}
exports.Teacher = Teacher;
//# sourceMappingURL=teacher.model.js.map