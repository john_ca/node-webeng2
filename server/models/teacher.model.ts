export class Teacher {

    constructor(public key, public prename, public surname, public username, public email, public admin, public password, public schoolKey, public googleId?) {

    }
}