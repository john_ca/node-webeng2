export class Note {
    constructor(public key, public teacherKey, public studentKey, public noteCategoryKey, public time, public text, public excused, public schoolKey) {
    }
}
