"use strict";
class SchoolClass {
    constructor(key, level, identifier, teacherKey, schoolKey) {
        this.key = key;
        this.level = level;
        this.identifier = identifier;
        this.teacherKey = teacherKey;
        this.schoolKey = schoolKey;
    }
}
exports.SchoolClass = SchoolClass;
//# sourceMappingURL=school-class.model.js.map