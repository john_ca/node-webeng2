"use strict";
class NoteCategory {
    constructor(key, name) {
        this.key = key;
        this.name = name;
    }
}
exports.NoteCategory = NoteCategory;
//# sourceMappingURL=note-category.model.js.map