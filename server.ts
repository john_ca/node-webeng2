import teacherRoute from "./server/routes/teacher";
import schoolRoute from "./server/routes/school";
import noteCategoryRoute from "./server/routes/noteCategory";
import studentRoute from "./server/routes/student";
import loginRoute from "./server/routes/login";
import noteRoute from "./server/routes/note";
import schoolClassRoute from "./server/routes/schoolClass";
import restrictedRoute from "./server/routes/restricted";
import redirectRoute from "./server/routes/redirect";
import googleRoute from "./server/routes/google";
import cookieSession = require("cookie-session");
const express = require('express');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');


const app = express();
app.enable('trust proxy');
const router = express.Router();


/*      Cookies     */
app.use(cookieParser());


var passport = require('passport');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

/*      GOOGLE Authentification      */

passport.use(new GoogleStrategy({
        clientID: '978304813164-2lh62f2l2p507ofr9en9cggo6pr8hvqc.apps.googleusercontent.com',
        clientSecret: 'DGQ-8GUV1AKWSI8e84tfp84v',
        callbackURL: "https://node-klassenbuch.appspot.com/auth/google/callback"
    },
    function (accessToken, refreshToken, profile, done) {
        let email = profile.emails[0].value;
        return done(null, profile.id);
    }
));
passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (user, done) {
    done(null, user);
});

app.use(passport.initialize());
app.use(passport.session());

/*      Session     */
app.use(cookieSession({
    name: 'session',
    secret: 'the answer to life the universe and everything',
    keys: ['test']
}));

/*      Body Parser     */
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

/*      Setup Routes        */

restrictedRoute(app);
redirectRoute(app);
googleRoute(app);
teacherRoute(app);
schoolRoute(app);
noteCategoryRoute(app);
noteRoute(app);
loginRoute(app);
schoolClassRoute(app);
studentRoute(app);

/*      Static Files        */
app.use(express.static('web'));


/*      Default Route       */
app.get('/', (req, res) => {
    res.sendFile(`${__dirname}/web/index.html`);
});

/*      Server Start        */
const PORT = process.env.PORT || 8080;

app.listen(PORT, () => {
    console.log(`App listening on port ${PORT}`);
});

